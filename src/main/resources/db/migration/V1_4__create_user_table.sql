CREATE TABLE IF NOT EXISTS ROLE (
    id          SERIAL,
    name        VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS USERS (
    id          SERIAL,
    name        VARCHAR(100) NOT NULL,
    email       VARCHAR(100) NOT NULL,
    password    VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE UNIQUE INDEX username_idx ON USERS(email);

CREATE TABLE IF NOT EXISTS USER_ROLE_RELATION (
    user_id     INTEGER,
    role_id     INTEGER,
    PRIMARY KEY(user_id, role_id),
    FOREIGN KEY(user_id) REFERENCES USERS(id),
    FOREIGN KEY(role_id) REFERENCES ROLE(id)
);

INSERT INTO ROLE (name) VALUES ('ADMIN');
INSERT INTO ROLE (name) VALUES ('USER');

INSERT INTO USERS (name, email, password)
VALUES ('admin', 'admin@email.com', '$2a$10$1ZxqVvoWqGHZj1steGgs8u8wgi9P0.QDRdXQxockyYWBTV.0sQfFq');

INSERT INTO USERS (name, email, password)
VALUES ('user', 'user@email.com', '$2a$10$1ZxqVvoWqGHZj1steGgs8u8wgi9P0.QDRdXQxockyYWBTV.0sQfFq');

INSERT INTO USERS (name, email, password)
VALUES ('andrew', 'fuskerr63@gmail.com', '$2a$10$1ZxqVvoWqGHZj1steGgs8u8wgi9P0.QDRdXQxockyYWBTV.0sQfFq');


INSERT INTO USER_ROLE_RELATION (user_id, role_id) VALUES (1, 1);
INSERT INTO USER_ROLE_RELATION (user_id, role_id) VALUES (2, 2);
INSERT INTO USER_ROLE_RELATION (user_id, role_id) VALUES (3, 1);
