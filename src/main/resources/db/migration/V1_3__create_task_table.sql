CREATE TABLE IF NOT EXISTS TASK (
    id SERIAL PRIMARY KEY,
    title VARCHAR NOT NULL,
    text VARCHAR NOT NULL,
    tag VARCHAR,
    status_name VARCHAR NOT NULL,
    assigned VARCHAR,
    board_id BIGINT NOT NULL,
    created_on TIMESTAMP NOT NULL DEFAULT NOW(),
    last_modify TIMESTAMP,
    FOREIGN KEY(status_name) REFERENCES STATUS(name),
    FOREIGN KEY(board_id) REFERENCES BOARD(id),
    FOREIGN KEY(board_id, status_name) REFERENCES BOARD_STATUS_RELATION(board_id, status_name)
);

CREATE INDEX task_tag_idx ON TASK(tag);