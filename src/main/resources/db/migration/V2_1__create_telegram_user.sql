CREATE TABLE IF NOT EXISTS TELEGRAM_ROLE (
    id          SERIAL,
    name        VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS TELEGRAM_USER (
    id          SERIAL,
    name        VARCHAR(100) NOT NULL,
    email       VARCHAR(100),
    sex         VARCHAR(100),
    age         INTEGER,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS TELEGRAM_USER_ROLE_RELATION (
    user_id     INTEGER,
    role_id     INTEGER,
    PRIMARY KEY(user_id, role_id),
    FOREIGN KEY(user_id) REFERENCES TELEGRAM_USER(id),
    FOREIGN KEY(role_id) REFERENCES TELEGRAM_ROLE(id)
);

INSERT INTO TELEGRAM_ROLE (name) VALUES ('ADMIN');
INSERT INTO TELEGRAM_ROLE (name) VALUES ('USER');

CREATE TABLE IF NOT EXISTS TELEGRAM_TAG (
    id          SERIAL,
    name        VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO TELEGRAM_TAG (name) VALUES ('Отдых');
INSERT INTO TELEGRAM_TAG (name) VALUES ('Спорт');
INSERT INTO TELEGRAM_TAG (name) VALUES ('Развлечение');

CREATE TABLE IF NOT EXISTS TELEGRAM_GROUP (
    id              SERIAL,
    name            VARCHAR(100),
    lat             VARCHAR(100),
    long            VARCHAR(100),
    address         VARCHAR(100),
    description     VARCHAR(100),
    tag             INTEGER,
    time            TIMESTAMP,
    creator         INTEGER,
    PRIMARY KEY (id),
    FOREIGN KEY(tag) REFERENCES TELEGRAM_TAG(id),
    FOREIGN KEY(creator) REFERENCES TELEGRAM_USER(id)
);

CREATE TABLE IF NOT EXISTS TELEGRAM_USER_GROUP_RELATION (
    user_id     INTEGER,
    group_id     INTEGER,
    PRIMARY KEY(user_id, group_id),
    FOREIGN KEY(user_id) REFERENCES TELEGRAM_USER(id),
    FOREIGN KEY(group_id) REFERENCES TELEGRAM_GROUP(id)
);
