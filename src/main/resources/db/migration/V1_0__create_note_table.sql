CREATE TABLE IF NOT EXISTS NOTE (
    id SERIAL PRIMARY KEY,
    title VARCHAR NOT NULL,
    text VARCHAR NOT NULL,
    tag VARCHAR,
    note_reference BIGINT,
    created_on TIMESTAMP NOT NULL DEFAULT NOW(),
    last_modify TIMESTAMP,
    FOREIGN KEY (note_reference) REFERENCES NOTE(id)
);

CREATE INDEX note_tag_idx ON NOTE(tag);
