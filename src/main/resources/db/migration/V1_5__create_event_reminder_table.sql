CREATE TABLE IF NOT EXISTS EVENT_REMINDER (
    id          SERIAL PRIMARY KEY,
    title       VARCHAR NOT NULL,
    text        VARCHAR NOT NULL,
    tag         VARCHAR,
    creator     BIGINT NOT NULL,
    created_on  TIMESTAMP NOT NULL DEFAULT NOW(),
    date        TIMESTAMP NOT NULL,
    last_modify TIMESTAMP,
    FOREIGN KEY(creator) REFERENCES USERS(id)
);

CREATE INDEX event_reminder_tag_idx ON EVENT_REMINDER(tag);

CREATE TABLE IF NOT EXISTS EVENT_REMINDER_USER_RELATION (
    user_id             INTEGER,
    event_reminder_id   INTEGER,
    PRIMARY KEY(user_id, event_reminder_id),
    FOREIGN KEY(user_id) REFERENCES USERS(id),
    FOREIGN KEY(event_reminder_id) REFERENCES EVENT_REMINDER(id)
);