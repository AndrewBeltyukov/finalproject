package com.gmail.fuskerr.finalproject.handler;

import com.gmail.fuskerr.finalproject.auth.SuchUsernameAlreadyExistsException;
import com.gmail.fuskerr.finalproject.board.BoardNotFoundException;
import com.gmail.fuskerr.finalproject.board.status.TaskStatusNotFoundException;
import com.gmail.fuskerr.finalproject.board.task.TaskNotFoundException;
import com.gmail.fuskerr.finalproject.note.NoteNotFoundException;
import com.gmail.fuskerr.finalproject.reminder.EventReminderNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class CommonErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({
            NoteNotFoundException.class,
            BoardNotFoundException.class,
            TaskNotFoundException.class,
            TaskStatusNotFoundException.class,
            EventReminderNotFoundException.class
    })
    protected ResponseEntity<Object> handleNotFound(RuntimeException exception) {
        ErrorResponse errorResponse = new ErrorResponse(false, exception.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({SuchUsernameAlreadyExistsException.class})
    protected ResponseEntity<Object> handleSuchUsernameAlreadyExists(SuchUsernameAlreadyExistsException ex) {
        ErrorResponse errorResponse = new ErrorResponse(false, ex.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Map<String, String> errorMap = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errorMap.put(fieldName, errorMessage);
        });
        return new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST);
    }

    public static record ErrorResponse(boolean success, String message) {
    }
}
