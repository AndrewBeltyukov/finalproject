package com.gmail.fuskerr.finalproject.reminder.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gmail.fuskerr.finalproject.auth.dto.UserResponseDto;

import java.time.LocalDateTime;
import java.util.List;

public record EventReminderResponseDto(
        Long id,
        String title,
        String text,
        String tag,
        @JsonFormat(pattern = "yyyy.MM.dd HH:mm:ss")
        LocalDateTime eventDate,
        UserResponseDto creator,
        List<UserResponseDto> subscribers,
        @JsonFormat(pattern = "yyyy.MM.dd HH:mm:ss")
        LocalDateTime createdOn,
        @JsonFormat(pattern = "yyyy.MM.dd HH:mm:ss")
        LocalDateTime lastModify) {
}
