package com.gmail.fuskerr.finalproject.reminder.notification;

import com.gmail.fuskerr.finalproject.reminder.model.EventReminder;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NotificationService {
    private final JavaMailSender sender;

    @Value("${spring.mail.username}")
    String emailFrom;

    public void sendNotification(String email, EventReminder eventReminder) throws MailException {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(email);
        message.setFrom(emailFrom);
        message.setSubject(eventReminder.getTitle());
        message.setText(eventReminder.getText());
        sender.send(message);
    }
}
