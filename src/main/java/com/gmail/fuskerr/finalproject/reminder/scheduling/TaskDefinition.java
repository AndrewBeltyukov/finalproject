package com.gmail.fuskerr.finalproject.reminder.scheduling;

import java.time.LocalDateTime;

public record TaskDefinition(LocalDateTime date, Long eventReminderId) {
}