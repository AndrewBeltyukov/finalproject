package com.gmail.fuskerr.finalproject.reminder.dto;

import com.gmail.fuskerr.finalproject.auth.dto.UserMapper;
import com.gmail.fuskerr.finalproject.reminder.model.EventReminder;

import java.util.List;
import java.util.stream.Collectors;

public class EventReminderMapper {

    public static EventReminder convertFromDto(EventReminderCreateDto dto) {
        return new EventReminder(
                dto.title(),
                dto.text(),
                dto.tag(),
                null,
                null,
                dto.eventDate(),
                null
        );
    }

    public static EventReminder convertFromDto(EventReminderUpdateDto dto) {
        return new EventReminder(
                dto.id(),
                dto.title(),
                dto.text(),
                dto.tag(),
                null,
                null,
                dto.eventDate(),
                null
        );
    }

    public static EventReminderResponseDto convertToDto(EventReminder eventReminder) {
        return new EventReminderResponseDto(
                eventReminder.getId(),
                eventReminder.getTitle(),
                eventReminder.getText(),
                eventReminder.getTag(),
                eventReminder.getEventDate(),
                UserMapper.convertToDto(eventReminder.getCreator()),
                UserMapper.convertToDto(eventReminder.getSubscribers()),
                eventReminder.getCreatedOn(),
                eventReminder.getLastModify()
        );
    }

    public static List<EventReminderResponseDto> convertToDto(List<EventReminder> eventReminders) {
        return eventReminders.stream().map(EventReminderMapper::convertToDto).collect(Collectors.toList());
    }
}
