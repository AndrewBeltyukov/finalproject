package com.gmail.fuskerr.finalproject.reminder.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public record EventReminderUpdateDto(
        @NotNull
        Long id,
        @NotBlank
        String title,
        @NotNull
        String text,
        @NotBlank
        String tag,
        @NotNull
        LocalDateTime eventDate) {
}
