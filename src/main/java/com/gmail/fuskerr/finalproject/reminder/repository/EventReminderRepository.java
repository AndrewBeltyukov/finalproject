package com.gmail.fuskerr.finalproject.reminder.repository;

import com.gmail.fuskerr.finalproject.reminder.model.EventReminder;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;
import java.util.Optional;

@Mapper
public interface EventReminderRepository {
    @Results(id = "selectEventReminder", value = {
            @Result(property = "id", column = "id", id = true),
            @Result(property = "title", column = "title"),
            @Result(property = "text", column = "text"),
            @Result(property = "tag", column = "tag"),
            @Result(property = "createdOn", column = "created_on"),
            @Result(property = "lastModify", column = "last_modify"),
            @Result(property = "eventDate", column = "date"),
            @Result(
                    property = "creator",
                    column = "creator",
                    one = @One(
                            select = "com.gmail.fuskerr.finalproject.auth.repository.UserRepository.findById",
                            fetchType = FetchType.EAGER
                    )
            ),
            @Result(
                    property = "subscribers",
                    column = "id",
                    many = @Many(
                            select = "com.gmail.fuskerr.finalproject.auth.repository.UserRepository.findUsersByEventReminderId",
                            fetchType = FetchType.EAGER
                    )
            )
    })
    @Select("SELECT id, title, text, tag, created_on, last_modify, date, creator " +
            "FROM EVENT_REMINDER WHERE id = #{id}")
    Optional<EventReminder> findById(@Param("id") Long id);

    @ResultMap("selectEventReminder")
    @Select("SELECT id, title, text, tag, created_on, last_modify, date, creator " +
            "FROM EVENT_REMINDER")
    List<EventReminder> findAll();

    @ResultMap("selectEventReminder")
    @Select("SELECT id, title, text, tag, created_on, last_modify, date, creator " +
            "FROM EVENT_REMINDER WHERE tag = #{tag}")
    List<EventReminder> findByTag(@Param("tag") String tag);

    @Insert("INSERT INTO EVENT_REMINDER(title, text, tag, created_on, last_modify, creator, date) " +
            "VALUES(#{title}, #{text}, #{tag}, now(), now(), #{creator.id}, #{eventDate})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void save(EventReminder eventReminder);

    @Update("UPDATE EVENT_REMINDER SET " +
            "title=#{title}, text=#{text}, tag=#{tag}, last_modify=now(), date=#{eventDate} " +
            "WHERE id=#{id}")
    Long update(EventReminder eventReminder);

    @Delete("DELETE FROM EVENT_REMINDER WHERE id = #{id}")
    Long deleteById(@Param("id") Long id);

    @Insert("INSERT INTO EVENT_REMINDER_USER_RELATION(user_id, event_reminder_id) " +
            "VALUES(#{user_id}, #{event_reminder_id})")
    void subscribeUser(@Param("user_id") Long userId, @Param("event_reminder_id") Long eventReminderId);

    @Insert("DELETE FROM EVENT_REMINDER_USER_RELATION WHERE event_reminder_id=#{event_reminder_id}")
    void unsubscribeAll(@Param("event_reminder_id") Long eventReminderId);
}
