package com.gmail.fuskerr.finalproject.reminder.scheduling;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;

import java.time.ZoneOffset;

@Service
@RequiredArgsConstructor
public class TaskSchedulingService {
    private final TaskScheduler taskScheduler;

    public void scheduleTask(SendEmailTask task) {
        taskScheduler.schedule(
                task,
                task.getTaskDefinition().date().toInstant(ZoneOffset.UTC));
    }
}
