package com.gmail.fuskerr.finalproject.reminder;

public class EventReminderNotFoundException extends RuntimeException {
    public EventReminderNotFoundException(String message) {
        super(message);
    }
}
