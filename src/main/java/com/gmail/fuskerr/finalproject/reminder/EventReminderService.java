package com.gmail.fuskerr.finalproject.reminder;

import com.gmail.fuskerr.finalproject.auth.model.User;
import com.gmail.fuskerr.finalproject.reminder.model.EventReminder;
import com.gmail.fuskerr.finalproject.reminder.notification.NotificationService;
import com.gmail.fuskerr.finalproject.reminder.repository.EventReminderRepository;
import com.gmail.fuskerr.finalproject.reminder.scheduling.SendEmailTask;
import com.gmail.fuskerr.finalproject.reminder.scheduling.TaskDefinition;
import com.gmail.fuskerr.finalproject.reminder.scheduling.TaskSchedulingService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EventReminderService {
    private final EventReminderRepository eventReminderRepository;
    private final NotificationService notificationService;
    private final TaskSchedulingService schedulingService;

    public EventReminder createAndGet(EventReminder eventReminder) {
        eventReminderRepository.save(eventReminder);
        createTaskToSendEmail(eventReminder);
        return  getById(eventReminder.getId());
    }

    public EventReminder getById(Long id) {
        return eventReminderRepository.findById(id)
                .orElseThrow(() -> new EventReminderNotFoundException("Not found event reminder with id: " + id));
    }

    public List<EventReminder> getAll() {
        return eventReminderRepository.findAll();
    }

    public List<EventReminder> getAllByTag(String tag) {
        return eventReminderRepository.findByTag(tag);
    }

    public boolean update(EventReminder eventReminder) {
        Long changedRows = eventReminderRepository.update(eventReminder);
        return isChange(changedRows);
    }

    @Transactional
    public boolean deleteById(Long id) {
        eventReminderRepository.unsubscribeAll(id);
        Long changedRows = eventReminderRepository.deleteById(id);
        return isChange(changedRows);
    }

    @Transactional
    public void subscribeUser(User user, Long id) {
        if(getById(id) != null) {
            eventReminderRepository.subscribeUser(user.getId(), id);
        }
    }

    // создать задачу на отправку письма на почту
    private void createTaskToSendEmail(EventReminder eventReminder) {
        TaskDefinition taskDefinition = new TaskDefinition(
                eventReminder.getEventDate(),
                eventReminder.getId());
        SendEmailTask task = new SendEmailTask(
                taskDefinition,
                this::getById,
                notificationService::sendNotification
        );
        schedulingService.scheduleTask(task);
    }

    private boolean isChange(Long rowNum) {
        return rowNum > 0;
    }
}
