package com.gmail.fuskerr.finalproject.reminder.model;

import com.gmail.fuskerr.finalproject.auth.model.User;
import com.gmail.fuskerr.finalproject.model.BaseForm;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

// сущность по типу "напоминалки" (напр. встреча, созвон и др.)
@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
public class EventReminder extends BaseForm {
    private LocalDateTime eventDate; // дата для напоминания
    private User creator;
    private List<User> subscribers; // подписавшиеся на это напоминание (участники)

    public EventReminder(
            String title,
            String text,
            String tag,
            LocalDateTime createdOn,
            LocalDateTime lastModify,
            LocalDateTime eventDate,
            User creator) {
        super(null, title, text, tag, createdOn, lastModify);
        this.eventDate = eventDate;
        this.creator = creator;
    }

    public EventReminder(
            Long id,
            String title,
            String text,
            String tag,
            LocalDateTime createdOn,
            LocalDateTime lastModify,
            LocalDateTime eventDate,
            User creator) {
        super(id, title, text, tag, createdOn, lastModify);
        this.eventDate = eventDate;
        this.creator = creator;
    }
}
