package com.gmail.fuskerr.finalproject.reminder;

import com.gmail.fuskerr.finalproject.auth.model.User;
import com.gmail.fuskerr.finalproject.dto.SuccessDto;
import com.gmail.fuskerr.finalproject.reminder.dto.EventReminderCreateDto;
import com.gmail.fuskerr.finalproject.reminder.dto.EventReminderMapper;
import com.gmail.fuskerr.finalproject.reminder.dto.EventReminderResponseDto;
import com.gmail.fuskerr.finalproject.reminder.dto.EventReminderUpdateDto;
import com.gmail.fuskerr.finalproject.reminder.model.EventReminder;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/reminder")
@RequiredArgsConstructor
public class EventReminderController {
    private final EventReminderService eventReminderService;

    @PostMapping
    public EventReminderResponseDto create(
            @Valid
            @RequestBody EventReminderCreateDto eventReminderCreateDto,
            Authentication authentication) {
        EventReminder eventReminder = EventReminderMapper.convertFromDto(eventReminderCreateDto);
        if (authentication.getPrincipal() instanceof User) {
            eventReminder.setCreator((User) authentication.getPrincipal());
        }
        EventReminder createdEventReminder = eventReminderService.createAndGet(eventReminder);
        return EventReminderMapper.convertToDto(createdEventReminder);
    }

    @GetMapping("/{id}")
    public EventReminderResponseDto get(@PathVariable("id") Long id) {
        EventReminder eventReminder = eventReminderService.getById(id);
        return EventReminderMapper.convertToDto(eventReminder);
    }

    @GetMapping
    public List<EventReminderResponseDto> getAll() {
        List<EventReminder> eventReminders = eventReminderService.getAll();
        return EventReminderMapper.convertToDto(eventReminders);
    }

    @GetMapping("/tag/{tag}")
    public List<EventReminderResponseDto> getByTag(
            @PathVariable("tag") String tag) {
        List<EventReminder> eventReminders = eventReminderService.getAllByTag(tag);
        return EventReminderMapper.convertToDto(eventReminders);
    }

    @PutMapping
    public SuccessDto update(
            @Valid
            @RequestBody EventReminderUpdateDto eventReminderUpdateDto) {
        EventReminder eventReminder = EventReminderMapper.convertFromDto(eventReminderUpdateDto);
        boolean success = eventReminderService.update(eventReminder);
        return new SuccessDto(success);
    }

    @DeleteMapping("/{id}")
    public SuccessDto delete(@PathVariable("id") Long id) {
        boolean success = eventReminderService.deleteById(id);
        return new SuccessDto(success);
    }

    @GetMapping("/{id}/subscribe")
    public void subscribe(
            @PathVariable("id") Long eventReminderId,
            Authentication authentication
    ) {
        if (authentication.getPrincipal() instanceof User) {
            eventReminderService.subscribeUser(
                    (User) authentication.getPrincipal(),
                    eventReminderId
            );
        }
    }
}
