package com.gmail.fuskerr.finalproject.reminder.scheduling;

import com.gmail.fuskerr.finalproject.auth.model.User;
import com.gmail.fuskerr.finalproject.reminder.model.EventReminder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

@Getter
@Setter
@RequiredArgsConstructor
public class SendEmailTask implements Runnable {
    private final TaskDefinition taskDefinition;
    private final Function<Long, EventReminder> getEventReminderByIdFunction; // callback на получение напоминалки по id
    private final BiConsumer<String, EventReminder> emailSender;

    @Override
    public void run() {
        EventReminder eventReminder = getEventReminderByIdFunction.apply(taskDefinition.eventReminderId());
        List<User> subscribers = eventReminder.getSubscribers();
        subscribers.stream()
                .map(User::getEmail)
                .forEach((email) -> emailSender.accept(email, eventReminder));
    }
}
