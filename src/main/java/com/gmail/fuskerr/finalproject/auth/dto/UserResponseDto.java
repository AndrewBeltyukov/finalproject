package com.gmail.fuskerr.finalproject.auth.dto;

public record UserResponseDto(Long id, String name, String email) {
}
