package com.gmail.fuskerr.finalproject.auth.dto;

import com.gmail.fuskerr.finalproject.auth.model.User;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class UserMapper {

    public static UserResponseDto convertToDto(User user) {
        return new UserResponseDto(
                user.getId(),
                user.getName(),
                user.getEmail()
        );
    }

    public static List<UserResponseDto> convertToDto(List<User> users) {
        if(users == null) {
            return Collections.emptyList();
        }
        return users.stream().map(UserMapper::convertToDto).collect(Collectors.toList());
    }
}
