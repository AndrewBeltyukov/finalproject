package com.gmail.fuskerr.finalproject.auth;

public class SuchUsernameAlreadyExistsException extends RuntimeException {

    public SuchUsernameAlreadyExistsException(String message) {
        super(message);
    }
}
