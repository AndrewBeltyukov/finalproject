package com.gmail.fuskerr.finalproject.auth.repository;

import com.gmail.fuskerr.finalproject.auth.UserRole;
import com.gmail.fuskerr.finalproject.auth.model.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Mapper
public interface UserRepository {

    @Results(id = "selectUser", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "email", column = "email"),
            @Result(property = "password", column = "password"),
            @Result(
                    property = "roles",
                    column = "id",
                    javaType = Set.class,
                    many = @Many(select = "getRolesByUserId")
            )
    })
    @Select("SELECT id, name, email, password FROM USERS WHERE email = #{email}")
    Optional<User> findByUsername(@Param("email") String username);

    @ResultMap("selectUser")
    @Select("SELECT id, name, email, password FROM USERS WHERE id = #{id}")
    Optional<User> findById(@Param("id") Long id);

    @Insert("INSERT INTO USERS(name, email, password) " +
            "VALUES(#{user.name}, #{user.email}, #{user.password})")
    @Options(useGeneratedKeys = true, keyProperty = "user.id")
    void save(@Param("user") User user);

    @Insert({"<script>",
            "INSERT INTO USER_ROLE_RELATION(user_id, role_id) ",
            "VALUES" +
                    "<foreach collection='roles' item='role'  open='' separator=',' close=''>",
            "(#{user_id}, (SELECT r.id FROM ROLE r WHERE r.name = #{role.name})) ",
            "</foreach>",
            "</script>"})
    void saveRolesToUser(@Param("user_id") Long userId, @Param("roles") Set<UserRole> roles);

    @Select("SELECT role.name FROM ROLE role " +
            "JOIN USER_ROLE_RELATION middle " +
            "ON role.id = middle.role_id " +
            "WHERE middle.user_id = #{user_id}")
    List<UserRole> getRolesByUserId(@Param("user_id") Long id);

    @ResultMap("selectUser")
    @Select("SELECT id, name, email, password FROM USERS u " +
            "JOIN EVENT_REMINDER_USER_RELATION middle " +
            "ON u.id = middle.user_id " +
            "WHERE middle.event_reminder_id = #{event_reminder_id}")
    List<User> findUsersByEventReminderId(@Param("event_reminder_id") Long id);
}

