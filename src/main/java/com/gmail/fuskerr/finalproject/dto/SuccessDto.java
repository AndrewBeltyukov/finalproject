package com.gmail.fuskerr.finalproject.dto;

public record SuccessDto(boolean success) {
}
