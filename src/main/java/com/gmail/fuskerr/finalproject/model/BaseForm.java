package com.gmail.fuskerr.finalproject.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class BaseForm {
    private Long id;
    private String title;
    private String text;
    private String tag;
    private LocalDateTime createdOn;
    private LocalDateTime lastModify;
}
