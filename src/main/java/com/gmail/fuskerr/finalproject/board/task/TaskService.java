package com.gmail.fuskerr.finalproject.board.task;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.gmail.fuskerr.finalproject.board.BoardService;
import com.gmail.fuskerr.finalproject.board.model.Board;
import com.gmail.fuskerr.finalproject.board.status.TaskStatusNotFoundException;
import com.gmail.fuskerr.finalproject.board.status.model.TaskStatus;
import com.gmail.fuskerr.finalproject.board.status.repository.BoardStatusRelationRepository;
import com.gmail.fuskerr.finalproject.board.task.model.Task;
import com.gmail.fuskerr.finalproject.board.task.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskService {
    private final TaskRepository taskRepository;
    private final BoardService boardService;
    private final BoardStatusRelationRepository boardStatusRelationRepository;

    @Transactional
    public Task createAndGet(Task task) {
        Board board = boardService.getById(task.getBoardId());
        if (!hasBoardRequiredTaskStatus(board.getId(), task.getTaskStatus())) {
            throw new TaskStatusNotFoundException(
                    "Not found status with name: " + task.getTaskStatus().name() +
                            " on board with id: " + board.getId());
        }
        taskRepository.save(task);
        return getById(task.getId());
    }

    public Task getById(Long id) {
        return taskRepository.findById(id)
                .orElseThrow(() -> new TaskNotFoundException("Not found task with id: " + id));
    }

    public Page<Task> getAllByTag(String tag, Integer page, Integer size) {
        PageHelper.startPage(page, size);
        return taskRepository.findByTag(tag);
    }

    public List<Task> getAllByBoardId(Long boardId) {
        return taskRepository.findByBoardId(boardId);
    }

    public boolean update(Task task) {
        Long changedRows = taskRepository.update(task);
        return isChanged(changedRows);
    }

    // переместить задачу с одной доски на другую доску
    @Transactional
    public boolean moveToAnotherBoard(Task task) {
        final Long anotherBoardId = task.getBoardId();
        Task taskFromDb = getById(task.getId());
        TaskStatus taskStatus = taskFromDb.getTaskStatus();
        // если на другой доске нет такого статуса, то выбрасываем исключение
        if (!hasBoardRequiredTaskStatus(anotherBoardId, taskStatus)) {
            throw new TaskStatusNotFoundException(
                    "Not found status with name: " + taskStatus.name() +
                            " on board with id: " + anotherBoardId);
        }
        Long changedRows = taskRepository.updateBoard(task.getId(), anotherBoardId);
        return isChanged(changedRows);
    }

    // переместить задачу в другой статус
    @Transactional
    public boolean moveToAnotherTaskStatus(Task task) {
        Task taskFromDb = getById(task.getId());
        Long boardId = taskFromDb.getBoardId();
        TaskStatus newTaskStatus = task.getTaskStatus();
        // если на текущей доске нет такого статуса, то выбрасываем исключение
        if (!hasBoardRequiredTaskStatus(boardId, newTaskStatus)) {
            throw new TaskStatusNotFoundException(
                    "Not found status with name: " + newTaskStatus.name() +
                            " on board with id: " + boardId);
        }
        Long changedRows = taskRepository.updateStatus(task.getId(), task.getTaskStatus());
        return isChanged(changedRows);
    }

    public boolean deleteById(Long id) {
        Long changedRows = taskRepository.deleteById(id);
        return isChanged(changedRows);
    }

    // определить имеется ли на доске нужный статус
    private boolean hasBoardRequiredTaskStatus(Long boardId, TaskStatus taskStatus) {
        List<TaskStatus> taskStatusesOnBoard = boardStatusRelationRepository.findAllTaskStatusesByBoardId(boardId);
        return taskStatusesOnBoard.contains(taskStatus);
    }

    private boolean isChanged(Long rowCount) {
        return rowCount > 0;
    }
}
