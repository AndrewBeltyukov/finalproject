package com.gmail.fuskerr.finalproject.board.status.repository;

import com.gmail.fuskerr.finalproject.board.status.model.TaskStatus;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface StatusRepository {
    @Results(id = "selectStatus", value = {
            @Result(property = "name", column = "name", id = true, typeHandler = StatusTypeHandler.class)
    })
    @Select("SELECT name FROM STATUS WHERE name = UPPER(#{name})")
    Optional<TaskStatus> findById(@Param("name") String name);

    @ResultMap("selectStatus")
    @Select("SELECT name FROM STATUS")
    List<TaskStatus> findAll();

    @Insert("INSERT INTO STATUS(name) " +
            "VALUES(UPPER(#{name}))")
    void save(TaskStatus taskStatus);

    @Delete("DELETE FROM STATUS WHERE name = UPPER(#{name})")
    Long deleteById(@Param("name") String name);
}
