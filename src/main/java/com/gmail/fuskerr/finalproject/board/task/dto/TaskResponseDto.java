package com.gmail.fuskerr.finalproject.board.task.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gmail.fuskerr.finalproject.board.status.dto.TaskStatusDto;

import java.time.LocalDateTime;

public record TaskResponseDto(
        Long id,
        String title,
        String text,
        String tag,
        TaskStatusDto status,
        @JsonFormat(pattern = "yyyy.MM.dd HH:mm:ss")
        LocalDateTime createdOn,
        @JsonFormat(pattern = "yyyy.MM.dd HH:mm:ss")
        LocalDateTime lastModify,
        String assignedName) {
}
