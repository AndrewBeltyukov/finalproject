package com.gmail.fuskerr.finalproject.board.status.repository;

import com.gmail.fuskerr.finalproject.board.status.model.TaskStatus;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedJdbcTypes(JdbcType.VARCHAR)
@MappedTypes(TaskStatus.class)
public class StatusTypeHandler extends BaseTypeHandler<TaskStatus> {
    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int columnIndex, TaskStatus taskStatus, JdbcType jdbcType) throws SQLException {
        preparedStatement.setString(columnIndex, taskStatus.name());
    }

    @Override
    public TaskStatus getNullableResult(ResultSet resultSet, String columnName) throws SQLException {
        return new TaskStatus(resultSet.getString(columnName));
    }

    @Override
    public TaskStatus getNullableResult(ResultSet resultSet, int columnIndex) throws SQLException {
        return new TaskStatus(resultSet.getString(columnIndex));
    }

    @Override
    public TaskStatus getNullableResult(CallableStatement callableStatement, int columnIndex) throws SQLException {
        return new TaskStatus(callableStatement.getString(columnIndex));
    }
}
