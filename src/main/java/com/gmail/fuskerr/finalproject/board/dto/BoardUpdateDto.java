package com.gmail.fuskerr.finalproject.board.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public record BoardUpdateDto(
        @NotNull
        Long id,
        @NotBlank
        String title,
        @NotBlank
        String text,
        @NotBlank
        String tag) {
}
