package com.gmail.fuskerr.finalproject.board.task.dto;

import com.gmail.fuskerr.finalproject.board.status.dto.TaskStatusDto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public record TaskCreateDto(
        @NotBlank
        String title,
        @NotNull
        String text,
        @NotBlank
        String tag,
        @NotNull
        TaskStatusDto status,
        String assignedName,
        Long boardId) {
}
