package com.gmail.fuskerr.finalproject.board.status;

import com.gmail.fuskerr.finalproject.board.status.model.TaskStatus;
import com.gmail.fuskerr.finalproject.board.status.repository.BoardStatusRelationRepository;
import com.gmail.fuskerr.finalproject.board.status.repository.StatusRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StatusService {
    private final StatusRepository statusRepository;
    private final BoardStatusRelationRepository relationRepository;

    public void create(TaskStatus taskStatus) {
        statusRepository.save(taskStatus);
    }

    public TaskStatus getByName(String name) {
        return statusRepository.findById(name.toUpperCase())
                .orElseThrow(() -> new TaskStatusNotFoundException("Not found status with name: " + name));
    }

    public List<TaskStatus> getAll() {
        return statusRepository.findAll();
    }

    public void addAllToBoard(List<TaskStatus> taskStatuses, Long boardId) {
        relationRepository.saveAllTaskStatusesToBoard(taskStatuses, boardId);
    }

    public boolean deleteByName(String name) {
        return statusRepository.deleteById(name.toUpperCase()) > 0;
    }
}
