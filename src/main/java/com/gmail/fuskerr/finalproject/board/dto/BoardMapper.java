package com.gmail.fuskerr.finalproject.board.dto;

import com.github.pagehelper.Page;
import com.gmail.fuskerr.finalproject.board.model.Board;
import com.gmail.fuskerr.finalproject.board.status.dto.TaskStatusMapper;

import java.util.List;
import java.util.stream.Collectors;

public class BoardMapper {

    public static Board convertFromDto(BoardCreateDto dto) {
        return new Board(
                dto.title(),
                dto.text(),
                dto.tag(),
                null,
                null,
                TaskStatusMapper.convertFromDto(dto.statuses())
        );
    }

    public static Board convertFromDto(BoardUpdateDto dto) {
        return new Board(
                dto.id(),
                dto.title(),
                dto.text(),
                dto.tag(),
                null,
                null,
                null
        );
    }

    public static BoardResponseDto convertToDto(Board board) {
        return new BoardResponseDto(
                board.getId(),
                board.getTitle(),
                board.getText(),
                board.getTag(),
                TaskStatusMapper.convertToDto(board.getTaskStatuses()),
                board.getCreatedOn(),
                board.getLastModify()
        );
    }

    public static Page<BoardResponseDto> convertToDto(Page<Board> boardPage) {
        List<Board> boards = boardPage.getResult();
        List<BoardResponseDto> gets = boards.stream().map(BoardMapper::convertToDto).collect(Collectors.toList());
        Page<BoardResponseDto> getPage = new Page<>(boardPage.getPageNum(), boardPage.getPageSize(), boardPage.isCount());
        getPage.addAll(gets);
        return getPage;
    }
}
