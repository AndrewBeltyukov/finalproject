package com.gmail.fuskerr.finalproject.board.task;

import com.github.pagehelper.Page;
import com.gmail.fuskerr.finalproject.board.task.dto.*;
import com.gmail.fuskerr.finalproject.board.task.model.Task;
import com.gmail.fuskerr.finalproject.dto.SuccessDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/board/{board_id}/task")
@RequiredArgsConstructor
public class TaskController {
    private final TaskService taskService;

    @PostMapping
    public TaskResponseDto create(
            @PathVariable("board_id") Long boardId,
            @Valid
            @RequestBody TaskCreateDto taskDto) {
        Task task = TaskMapper.convertFromDto(taskDto);
        task.setBoardId(boardId);
        Task createdTask = taskService.createAndGet(task);
        return TaskMapper.convertToDto(createdTask);
    }

    @GetMapping("/{id}")
    public TaskResponseDto getById(@PathVariable("id") Long id) {
        Task task = taskService.getById(id);
        return TaskMapper.convertToDto(task);
    }

    @GetMapping("/tag/{tag}")
    public Page<TaskResponseDto> getByTagPage(
            @RequestParam(name = "page", defaultValue = "0") Integer page,
            @RequestParam(name = "size", defaultValue = "5") Integer size,
            @PathVariable("tag") String tag) {
        Page<Task> taskPage = taskService.getAllByTag(tag, page, size);
        return TaskMapper.convertToDto(taskPage);
    }

    @GetMapping
    public List<TaskResponseDto> getOnBoard(@PathVariable("board_id") Long boardId) {
        List<Task> tasks = taskService.getAllByBoardId(boardId);
        return TaskMapper.convertToDto(tasks);
    }

    @PatchMapping("/migrate")
    public SuccessDto migrateToBoard(
            @Valid
            @RequestBody TaskMigrateDto taskDTO) {
        Task task = TaskMapper.convertFromDto(taskDTO);
        boolean success = taskService.moveToAnotherBoard(task);
        return new SuccessDto(success);
    }

    @PatchMapping("/status")
    public SuccessDto changeStatus(
            @Valid
            @RequestBody TaskUpdateStatusDto taskDTO) {
        Task task = TaskMapper.convertFromDto(taskDTO);
        boolean success = taskService.moveToAnotherTaskStatus(task);
        return new SuccessDto(success);
    }

    @PutMapping
    public SuccessDto update(
            @Valid
            @RequestBody TaskUpdateDto taskDTO) {
        Task task = TaskMapper.convertFromDto(taskDTO);
        boolean success = taskService.update(task);
        return new SuccessDto(success);
    }

    @DeleteMapping("/{id}")
    public SuccessDto delete(@PathVariable("id") Long id) {
        boolean success = taskService.deleteById(id);
        return new SuccessDto(success);
    }
}
