package com.gmail.fuskerr.finalproject.board.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gmail.fuskerr.finalproject.board.status.dto.TaskStatusDto;

import java.time.LocalDateTime;
import java.util.List;

public record BoardResponseDto(
        Long id,
        String title,
        String text,
        String tag,
        List<TaskStatusDto> statuses,
        @JsonFormat(pattern = "yyyy.MM.dd HH:mm:ss")
        LocalDateTime createdOn,
        @JsonFormat(pattern = "yyyy.MM.dd HH:mm:ss")
        LocalDateTime lastModify) {
}
