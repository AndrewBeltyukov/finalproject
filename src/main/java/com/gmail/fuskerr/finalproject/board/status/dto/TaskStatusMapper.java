package com.gmail.fuskerr.finalproject.board.status.dto;

import com.gmail.fuskerr.finalproject.board.status.model.TaskStatus;

import java.util.List;
import java.util.stream.Collectors;

public class TaskStatusMapper {

    public static TaskStatusDto convertToDto(TaskStatus taskStatus) {
        return new TaskStatusDto(taskStatus.name());
    }

    public static List<TaskStatusDto> convertToDto(List<TaskStatus> taskStatusList) {
        return taskStatusList.stream()
                .map(TaskStatusMapper::convertToDto)
                .collect(Collectors.toList());
    }

    public static TaskStatus convertFromDto(TaskStatusDto dto) {
        return new TaskStatus(dto.name());
    }

    public static List<TaskStatus> convertFromDto(List<TaskStatusDto> dtoList) {
        return dtoList.stream()
                .map(TaskStatusMapper::convertFromDto)
                .collect(Collectors.toList());
    }
}
