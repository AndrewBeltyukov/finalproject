package com.gmail.fuskerr.finalproject.board;

import com.github.pagehelper.Page;
import com.gmail.fuskerr.finalproject.board.dto.*;
import com.gmail.fuskerr.finalproject.board.model.Board;
import com.gmail.fuskerr.finalproject.dto.SuccessDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/board")
@RequiredArgsConstructor
public class BoardController {
    private final BoardService boardService;

    @PostMapping
    public BoardResponseDto create(
            @Valid
            @RequestBody BoardCreateDto boardDTO) {
        Board board = BoardMapper.convertFromDto(boardDTO);
        Board createdBoard = boardService.createAndGet(board);
        return BoardMapper.convertToDto(createdBoard);
    }

    @GetMapping("/{id}")
    public BoardResponseDto get(@PathVariable("id") Long id) {
        Board board = boardService.getById(id);
        return BoardMapper.convertToDto(board);
    }

    @GetMapping
    public Page<BoardResponseDto> getPage(
            @RequestParam(name = "page", defaultValue = "0") Integer page,
            @RequestParam(name = "size", defaultValue = "5") Integer size) {
        Page<Board> boardPage = boardService.getByPage(page, size);
        return BoardMapper.convertToDto(boardPage);
    }

    @GetMapping("/tag/{tag}")
    public Page<BoardResponseDto> getPageByTag(
            @RequestParam(name = "page", defaultValue = "0") Integer page,
            @RequestParam(name = "size", defaultValue = "5") Integer size,
            @PathVariable("tag") String tag) {
        return BoardMapper.convertToDto(boardService.getByTagPage(tag, page, size));
    }

    @PutMapping
    public SuccessDto update(
            @Valid
            @RequestBody BoardUpdateDto boardDTO) {
        Board board = BoardMapper.convertFromDto(boardDTO);
        boolean success = boardService.update(board);
        return new SuccessDto(success);
    }

    @DeleteMapping("/{id}")
    public SuccessDto delete(@PathVariable("id") Long id) {
        boolean success = boardService.deleteById(id);
        return new SuccessDto(success);
    }
}
