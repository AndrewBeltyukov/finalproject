package com.gmail.fuskerr.finalproject.board.model;

import com.gmail.fuskerr.finalproject.board.status.model.TaskStatus;
import com.gmail.fuskerr.finalproject.model.BaseForm;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
public class Board extends BaseForm {
    private List<TaskStatus> taskStatuses;

    public Board(
            String title,
            String text,
            String tag,
            LocalDateTime createdOn,
            LocalDateTime lastModify,
            List<TaskStatus> taskStatuses) {
        super(null, title, text, tag, createdOn, lastModify);
        this.taskStatuses = taskStatuses;
    }

    public Board(
            Long id,
            String title,
            String text,
            String tag,
            LocalDateTime createdOn,
            LocalDateTime lastModify,
            List<TaskStatus> taskStatuses) {
        super(id, title, text, tag, createdOn, lastModify);
        this.taskStatuses = taskStatuses;
    }
}
