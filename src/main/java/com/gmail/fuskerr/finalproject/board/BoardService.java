package com.gmail.fuskerr.finalproject.board;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.gmail.fuskerr.finalproject.board.model.Board;
import com.gmail.fuskerr.finalproject.board.repository.BoardRepository;
import com.gmail.fuskerr.finalproject.board.status.StatusService;
import com.gmail.fuskerr.finalproject.board.status.repository.BoardStatusRelationRepository;
import com.gmail.fuskerr.finalproject.board.status.model.TaskStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Predicate;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;
    private final BoardStatusRelationRepository relationRepository;

    private final StatusService statusService;

    @Transactional
    public Board createAndGet(Board board) {
        List<TaskStatus> taskStatuses = statusService.getAll();
        board.getTaskStatuses().stream()
                .filter(Predicate.not(taskStatuses::contains))
                .forEach(statusService::create);
        boardRepository.save(board);
        relationRepository.saveAllTaskStatusesToBoard(board.getTaskStatuses(), board.getId());
        return getById(board.getId());
    }

    public Board getById(Long id) {
        return boardRepository.findById(id)
                .orElseThrow(() -> new BoardNotFoundException("Not found board with id: " + id));
    }

    public Page<Board> getByPage(Integer page, Integer size) {
        PageHelper.startPage(page, size);
        return boardRepository.findAll();
    }

    public Page<Board> getByTagPage(String tag, Integer page, Integer size) {
        PageHelper.startPage(page, size);
        return boardRepository.findByTag(tag);
    }

    public boolean update(Board board) {
        Long result = boardRepository.update(board);
        return isChanged(result);
    }

    @Transactional
    public boolean deleteById(Long id) {
        relationRepository.deleteTaskStatusesFromBoard(id);
        Long result = boardRepository.deleteById(id);
        return isChanged(result);
    }

    private boolean isChanged(Long rowCount) {
        return rowCount > 0;
    }
}
