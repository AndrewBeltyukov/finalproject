package com.gmail.fuskerr.finalproject.board.task.repository;

import com.github.pagehelper.Page;
import com.gmail.fuskerr.finalproject.board.status.model.TaskStatus;
import com.gmail.fuskerr.finalproject.board.status.repository.StatusTypeHandler;
import com.gmail.fuskerr.finalproject.board.task.model.Task;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface TaskRepository {
    @Results(id = "selectTask", value = {
            @Result(property = "id", column = "id", id = true),
            @Result(property = "title", column = "title"),
            @Result(property = "text", column = "text"),
            @Result(property = "tag", column = "tag"),
            @Result(property = "taskStatus", column = "status_name", typeHandler = StatusTypeHandler.class),
            @Result(property = "assignedName", column = "assigned"),
            @Result(property = "boardId", column = "board_id"),
            @Result(property = "createdOn", column = "created_on"),
            @Result(property = "lastModify", column = "last_modify")
    })
    @Select("SELECT id, title, text, tag, status_name, assigned, board_id, created_on, last_modify " +
            "FROM TASK WHERE id = #{task_id}")
    Optional<Task> findById(@Param("task_id") Long id);

    @ResultMap("selectTask")
    @Select("SELECT id, title, text, tag, status_name, assigned, board_id, created_on, last_modify " +
            "FROM TASK WHERE tag = #{tag}")
    Page<Task> findByTag(@Param("tag") String tag);

    @ResultMap("selectTask")
    @Select("SELECT id, title, text, tag, status_name, assigned, board_id, created_on, last_modify " +
            "FROM TASK WHERE board_id = #{board_id}")
    List<Task> findByBoardId(@Param("board_id") Long id);

    @Insert("INSERT INTO TASK(title, text, tag, status_name, assigned, board_id, created_on, last_modify) " +
            "VALUES(#{title}, #{text}, #{tag}, #{taskStatus}, #{assignedName}, #{boardId}, now(), now())")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void save(Task task);

    @Delete("DELETE FROM TASK WHERE id = #{task_id}")
    Long deleteById(@Param("task_id") Long id);

    @Update("UPDATE TASK SET " +
            "title=#{title}, text=#{text}, tag=#{tag}, assigned=#{assignedName}, last_modify=now() " +
            "WHERE id=#{id}")
    Long update(Task task);

    @Update("UPDATE TASK SET " +
            "board_id=#{board_id} " +
            "WHERE id=#{task_id}")
    Long updateBoard(@Param("task_id") Long id, @Param("board_id") Long boardId);

    @Update("UPDATE TASK SET " +
            "status_name=#{status.name} " +
            "WHERE id=#{task_id}")
    Long updateStatus(@Param("task_id") Long id, @Param("status") TaskStatus taskStatus);
}
