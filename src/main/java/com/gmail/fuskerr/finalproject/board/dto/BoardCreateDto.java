package com.gmail.fuskerr.finalproject.board.dto;

import com.gmail.fuskerr.finalproject.board.status.dto.TaskStatusDto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public record BoardCreateDto(
        @NotBlank
        String title,
        @NotNull
        String text,
        @NotBlank
        String tag,
        @NotEmpty
        List<TaskStatusDto> statuses) {
}
