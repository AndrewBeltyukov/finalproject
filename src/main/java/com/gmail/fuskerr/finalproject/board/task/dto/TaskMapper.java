package com.gmail.fuskerr.finalproject.board.task.dto;

import com.github.pagehelper.Page;
import com.gmail.fuskerr.finalproject.board.status.dto.TaskStatusMapper;
import com.gmail.fuskerr.finalproject.board.task.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public class TaskMapper {

    public static Task convertFromDto(TaskCreateDto dto) {
        return new Task(
                dto.title(),
                dto.text(),
                dto.tag(),
                TaskStatusMapper.convertFromDto(dto.status()),
                dto.assignedName(),
                dto.boardId(),
                null,
                null
        );
    }

    public static Task convertFromDto(TaskUpdateDto dto) {
        return new Task(
                dto.id(),
                dto.title(),
                dto.text(),
                dto.tag(),
                null,
                dto.assignedName(),
                null,
                null,
                null
        );
    }

    public static Task convertFromDto(TaskMigrateDto dto) {
        return new Task(
                dto.id(),
                null,
                null,
                null,
                null,
                null,
                dto.boardId(),
                null,
                null
        );
    }

    public static Task convertFromDto(TaskUpdateStatusDto dto) {
        return new Task(
                dto.id(),
                null,
                null,
                null,
                TaskStatusMapper.convertFromDto(dto.status()),
                null,
                null,
                null,
                null
        );
    }

    public static TaskResponseDto convertToDto(Task task) {
        return new TaskResponseDto(
                task.getId(),
                task.getTitle(),
                task.getText(),
                task.getTag(),
                TaskStatusMapper.convertToDto(task.getTaskStatus()),
                task.getCreatedOn(),
                task.getLastModify(),
                task.getAssignedName()
        );
    }

    public static Page<TaskResponseDto> convertToDto(Page<Task> taskPage) {
        List<Task> tasks = taskPage.getResult();
        List<TaskResponseDto> gets = tasks.stream().map(TaskMapper::convertToDto).collect(Collectors.toList());
        Page<TaskResponseDto> getPage = new Page<>(taskPage.getPageNum(), taskPage.getPageSize());
        getPage.addAll(gets);
        return getPage;
    }

    public static List<TaskResponseDto> convertToDto(List<Task> taskList) {
        return taskList.stream()
                .map(TaskMapper::convertToDto)
                .collect(Collectors.toList());
    }
}
