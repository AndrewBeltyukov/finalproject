package com.gmail.fuskerr.finalproject.board.task.dto;

import com.gmail.fuskerr.finalproject.board.status.dto.TaskStatusDto;

import javax.validation.constraints.NotNull;

public record TaskUpdateStatusDto(@NotNull Long id, @NotNull TaskStatusDto status) {
}
