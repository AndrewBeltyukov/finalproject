package com.gmail.fuskerr.finalproject.board.task.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public record TaskUpdateDto(
        @NotNull
        Long id,
        @NotBlank
        String title,
        @NotNull
        String text,
        @NotBlank
        String tag,
        String assignedName) {
}
