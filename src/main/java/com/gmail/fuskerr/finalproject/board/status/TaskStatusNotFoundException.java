package com.gmail.fuskerr.finalproject.board.status;

public class TaskStatusNotFoundException extends RuntimeException {
    public TaskStatusNotFoundException(String message) {
        super(message);
    }
}
