package com.gmail.fuskerr.finalproject.board.status.dto;

public record TaskStatusDto(String name) {
}
