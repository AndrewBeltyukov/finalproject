package com.gmail.fuskerr.finalproject.board.task.model;

import com.gmail.fuskerr.finalproject.board.status.model.TaskStatus;
import com.gmail.fuskerr.finalproject.model.BaseForm;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
public class Task extends BaseForm {
    private TaskStatus taskStatus;
    private String assignedName;
    private Long boardId;

    public Task(
            String title,
            String text,
            String tag,
            TaskStatus taskStatus,
            String assignedName,
            Long boardId,
            LocalDateTime createdOn,
            LocalDateTime lastModify) {
        super(null, title, text, tag, createdOn, lastModify);
        this.taskStatus = taskStatus;
        this.assignedName = assignedName;
        this.boardId = boardId;
    }

    public Task(
            Long id,
            String title,
            String text,
            String tag,
            TaskStatus taskStatus,
            String assignedName,
            Long boardId,
            LocalDateTime createdOn,
            LocalDateTime lastModify) {
        super(id, title, text, tag, createdOn, lastModify);
        this.taskStatus = taskStatus;
        this.assignedName = assignedName;
        this.boardId = boardId;
    }
}
