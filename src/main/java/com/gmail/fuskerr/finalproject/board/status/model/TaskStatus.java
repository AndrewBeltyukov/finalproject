package com.gmail.fuskerr.finalproject.board.status.model;

// статус для задач на доске (NEW, ACTIVE И Т.П.)
public record TaskStatus(String name) {
    public TaskStatus(String name) {
        this.name = name.toUpperCase();
    }
}
