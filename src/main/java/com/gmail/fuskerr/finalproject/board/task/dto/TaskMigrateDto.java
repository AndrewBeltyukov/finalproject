package com.gmail.fuskerr.finalproject.board.task.dto;

import javax.validation.constraints.NotNull;

// модель для перемещения задачи на другую доску
public record TaskMigrateDto(@NotNull Long id, @NotNull Long boardId) {
}
