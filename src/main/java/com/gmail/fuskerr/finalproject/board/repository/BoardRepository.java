package com.gmail.fuskerr.finalproject.board.repository;

import com.github.pagehelper.Page;
import com.gmail.fuskerr.finalproject.board.model.Board;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;
import java.util.Optional;

@Mapper
public interface BoardRepository {
    @Results(id = "selectBoard", value = {
            @Result(property = "id", column = "id", id = true),
            @Result(property = "title", column = "title"),
            @Result(property = "text", column = "text"),
            @Result(property = "tag", column = "tag"),
            @Result(property = "createdOn", column = "created_on", typeHandler = LocalDateTypeHandler.class),
            @Result(property = "lastModify", column = "last_modify", typeHandler = LocalDateTypeHandler.class),
            @Result(
                    property = "taskStatuses",
                    javaType = List.class,
                    column = "id",
                    many = @Many(
                            select = "com.gmail.fuskerr.finalproject.board.status.repository.BoardStatusRelationRepository.findAllTaskStatusesByBoardId",
                            fetchType = FetchType.EAGER
                    )
            )
    })
    @Select("SELECT id, title, text, tag, created_on, last_modify FROM BOARD WHERE id = #{board_id}")
    Optional<Board> findById(@Param("board_id") Long id);

    @ResultMap("selectBoard")
    @Select("SELECT id, title, text, tag, created_on, last_modify FROM BOARD")
    Page<Board> findAll();

    @ResultMap("selectBoard")
    @Select("SELECT id, title, text, tag, created_on, last_modify FROM BOARD WHERE tag = #{tag}")
    Page<Board> findByTag(@Param("tag") String tag);

    @Insert("INSERT INTO BOARD(title, text, tag, created_on, last_modify) " +
            "VALUES(#{title}, #{text}, #{tag}, now(), now())")
    @ResultMap("selectBoard")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    Long save(Board board);

    @Update("UPDATE BOARD SET " +
            "title=#{title}, text=#{text}, tag=#{tag}, last_modify=now() " +
            "WHERE id =#{id}")
    Long update(Board board);

    @Delete("DELETE FROM BOARD WHERE id = #{board_id}")
    Long deleteById(@Param("board_id") Long id);
}
