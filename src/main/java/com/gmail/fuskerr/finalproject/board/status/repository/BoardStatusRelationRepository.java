package com.gmail.fuskerr.finalproject.board.status.repository;

import com.gmail.fuskerr.finalproject.board.status.model.TaskStatus;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BoardStatusRelationRepository {
    @Insert({
            "<script>",
            "INSERT INTO BOARD_STATUS_RELATION",
            "(board_id, status_name)",
            "VALUES" +
                    "<foreach item='status' collection='statuses' open='' separator=',' close=''>" +
                        "(" +
                        "#{board_id,jdbcType=BIGINT},#{status.name,jdbcType=VARCHAR}" +
                        ")",
                    "</foreach>",
            "</script>"})
    void saveAllTaskStatusesToBoard(@Param("statuses") List<TaskStatus> taskStatuses, @Param("board_id") Long boardId);

    @Results(id = "selectStatus", value = {
            @Result(property = "name", column = "name", id = true)
    })
    @Select("SELECT name FROM STATUS s " +
            "JOIN BOARD_STATUS_RELATION middle " +
            "ON s.name = middle.status_name " +
            "WHERE middle.board_id = #{board_id}")
    List<TaskStatus> findAllTaskStatusesByBoardId(@Param("board_id") Long boardId);

    @Delete("DELETE FROM BOARD_STATUS_RELATION WHERE board_id = #{board_id}")
    Long deleteTaskStatusesFromBoard(@Param("board_id") Long id);
}
