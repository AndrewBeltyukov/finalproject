package com.gmail.fuskerr.finalproject.telegram.group;

import com.gmail.fuskerr.finalproject.telegram.auth.model.TelegramUser;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Group {
    private Long id;
    private String name;
    private String latitude;
    private String longitude;
    private String address;
    private String description;
    private TelegramTag tag;
    private LocalDateTime time;
    private TelegramUser creator;
    private List<TelegramUser> users;

    public Group(
            Long id,
            String name,
            String latitude,
            String longitude,
            String address,
            String description,
            TelegramTag tag,
            LocalDateTime time,
            TelegramUser creator) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
        this.description = description;
        this.tag = tag;
        this.time = time;
        this.creator = creator;
    }

    public Group(TelegramUser creator) {
        this.creator = creator;
    }

    @Override
    public String toString() {
        return "Группа" +
                "\nНазвание: " + (name != null ? name : "Не задано") +
                "\nДолгота: " + (latitude != null ? latitude : "Не задано") +
                "\nШирота: " + (longitude != null ? longitude : "Не задано") +
                "\nАдрес: " + (address != null ? address : "Не задано") +
                "\nОписание: " + (description != null ? description : "Не задано") +
                "\nТег: " + (tag != null ? tag.getName() : "Не задано") +
                "\nВремя: " + (time != null ? time : "Не задано") +
                "\nСоздатель: " + (creator != null ? creator.getName() : "Не задано");
    }
}
