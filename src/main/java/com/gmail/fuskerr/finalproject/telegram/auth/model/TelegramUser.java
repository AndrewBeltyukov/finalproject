package com.gmail.fuskerr.finalproject.telegram.auth.model;


import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Setter
@Getter
public class TelegramUser {
        private Long id;
        private String name;
        private String email;
        private String sex;
        private Integer age;
        private Set<TelegramUserRole> roles;

    public TelegramUser(Long id, String name, String email, String sex, Integer age, Set<TelegramUserRole> roles) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.sex = sex;
        this.age = age;
        this.roles = roles;
    }

    public TelegramUser(Long id, String name, String email, String sex, Integer age) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.sex = sex;
        this.age = age;
        this.roles = roles;
    }

    public TelegramUser(String name) {
        this.name = name;
    }

    public TelegramUser(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
