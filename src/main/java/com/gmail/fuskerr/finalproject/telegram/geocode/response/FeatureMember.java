package com.gmail.fuskerr.finalproject.telegram.geocode.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FeatureMember{
    @JsonProperty("GeoObject")
    public GeoObject geoObject;
}