package com.gmail.fuskerr.finalproject.telegram.group;

import com.gmail.fuskerr.finalproject.telegram.auth.model.TelegramUser;
import org.apache.ibatis.annotations.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Mapper
public interface GroupRepository {
    @Results(id = "selectGroup", value = {
            @Result(property = "id", column = "id", javaType = Long.class),
            @Result(property = "name", column = "name"),
            @Result(property = "latitude", column = "lat"),
            @Result(property = "longitude", column = "long"),
            @Result(property = "address", column = "address"),
            @Result(property = "description", column = "description"),
            @Result(property = "tag", column = "tag",
                    javaType = TelegramTag.class,
                    one = @One(select = "findTagById")
            ),
            @Result(property = "time", column = "time",
                    javaType = LocalDateTime.class
            ),
            @Result(property = "creator", column = "creator",
                    javaType = TelegramUser.class,
                    one = @One(select="com.gmail.fuskerr.finalproject.telegram.auth.repository.TelegramUserRepository.findById")),
            @Result(
                    property = "users",
                    column = "id",
                    javaType = List.class,
                    many = @Many(select = "com.gmail.fuskerr.finalproject.telegram.auth.repository.TelegramUserRepository.getUsersByGroupId")
            )
    })
    @Select("SELECT * FROM TELEGRAM_GROUP WHERE creator = #{creator_id}")
    Optional<Group> findByUserId(@Param("creator_id") Long id);

    @ResultMap("selectGroup")
    @Select("SELECT * FROM TELEGRAM_GROUP WHERE tag = #{tag} AND description LIKE CONCAT('%',#{description},'%')")
    List<Group> findByTagAndDescriptionLike(@Param("tag") Long tag, @Param("description") String description);

    @Insert("INSERT INTO TELEGRAM_GROUP(name, lat, long, address, description, tag, time, creator) " +
            "VALUES(#{group.name}, #{group.latitude}, #{group.longitude}, #{group.address}, #{group.description}," +
            "#{group.tag}, #{group.time}, #{group.creator.id})")
    @Options(useGeneratedKeys = true, keyProperty = "group.id")
    void save(@Param("group") Group group);

    @Update("UPDATE TELEGRAM_GROUP SET " +
            "name=COALESCE(#{name}, name), " +
            "lat=COALESCE(#{latitude}, lat), " +
            "long=COALESCE(#{longitude}, long), " +
            "address=COALESCE(#{address}, address), " +
            "description=COALESCE(#{description}, description), " +
            "tag=COALESCE(#{tag.id}, tag), " +
            "time=COALESCE(#{time}, time), " +
            "creator=COALESCE(#{creator.id}, creator)" +
            "WHERE id =#{id}")
    Long update(Group group);

    @Results(id = "selectTag", value = {
            @Result(property = "id", column = "id", javaType = Long.class),
            @Result(property = "name", column = "name")
    })
    @Select("SELECT id, name FROM TELEGRAM_TAG WHERE id = #{id}")
    TelegramTag findTagById(@Param("id") Long id);

    @ResultMap("selectTag")
    @Select("SELECT id, name FROM TELEGRAM_TAG WHERE name = #{name}")
    Optional<TelegramTag> findTagByName(@Param("name") String name);

    @Delete("DELETE FROM TELEGRAM_GROUP WHERE creator = #{creator_id}")
    Long deleteById(@Param("creator_id") Long id);
}
