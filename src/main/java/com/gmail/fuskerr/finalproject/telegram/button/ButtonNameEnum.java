package com.gmail.fuskerr.finalproject.telegram.button;

public enum ButtonNameEnum {
    AUTH_BUTTON("Авторизоваться"),
    REGISTER_BUTTON("Начать регистрацию"),
    EMAIL_COMMAND("/email"),
    AGE_COMMAND("/age"),
    SEX_COMMAND("/sex"),

    CREATE_GROUP_BUTTON("Создать группу"),
    FIND_GROUP_BUTTON("Найти группу"),
    GROUP_NAME_COMMAND("/name"),
    GROUP_ADDRESS_COMMAND("/address"),
    GROUP_DESCRIPTION_COMMAND("/desc"),
    GROUP_TAG_COMMAND("/tag"),
    GROUP_TIME_COMMAND("/time"),
    FIND_COMMAND("/find"),

    DELETE_GROUP_BUTTON("Удалить группу"),
    CANCEL_BUTTON("Отменить"),

    START_BUTTON("/start"),
    UNKNOWN_BUTTON("Неизвестная кнопка");

    private final String buttonName;

    ButtonNameEnum(String buttonName) {
        this.buttonName = buttonName;
    }

    public String getButtonName() {
        return buttonName;
    }

    public static ButtonNameEnum getByName(String buttonName) {
        for (ButtonNameEnum element : values()) {
            if (buttonName.contains(element.getButtonName())) {
                return element;
            }
        }
        return ButtonNameEnum.UNKNOWN_BUTTON;
    }
}
