package com.gmail.fuskerr.finalproject.telegram.group;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TelegramTag {
    private Long id;
    private String name;
}
