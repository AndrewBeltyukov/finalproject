package com.gmail.fuskerr.finalproject.telegram.geocode;

import com.gmail.fuskerr.finalproject.telegram.geocode.response.Point;
import com.gmail.fuskerr.finalproject.telegram.geocode.response.Response;
import lombok.RequiredArgsConstructor;
import org.flywaydb.core.internal.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class YandexService {
    private final RestTemplate restTemplate = new RestTemplate();
    private final String BASE_URL =
            "https://geocode-maps.yandex.ru/1.x?format=json&lang=ru_RU&kind=house&apikey=c4ae7cc9-6b40-43d7-830e-88c8687e4561&geocode=";

    public Pair<String, String> getLatLongByAddress(String address) {
        Response response = restTemplate.getForObject(BASE_URL + address, Response.class);
        Point point = response.getResponseInner().getGeoObjectCollection().getFeatureMember().get(0).getGeoObject().getPoint();
        String[] latLongPoint = point.getPos().split("\\s+");
        return Pair.of(latLongPoint[0], latLongPoint[1]);
    }
}
