package com.gmail.fuskerr.finalproject.telegram.auth;

import com.gmail.fuskerr.finalproject.telegram.auth.model.TelegramUser;
import com.gmail.fuskerr.finalproject.telegram.auth.repository.TelegramUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TelegramUserService {
    private final TelegramUserRepository telegramUserRepository;

    public void saveUser(TelegramUser user) {
        telegramUserRepository.save(user);
    }

    public Optional<TelegramUser> getUserByName(String name) {
        return telegramUserRepository.findByUsername(name);
    }

    public void update(TelegramUser user) {
        telegramUserRepository.update(user);
    }
}
