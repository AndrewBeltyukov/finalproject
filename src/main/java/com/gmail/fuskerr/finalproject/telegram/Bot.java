package com.gmail.fuskerr.finalproject.telegram;

import com.gmail.fuskerr.finalproject.telegram.handler.MessageHandler;
import com.gmail.fuskerr.finalproject.telegram.service.BotService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updates.SetWebhook;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.starter.SpringWebhookBot;

@Component
@Setter
@Getter
public class Bot extends SpringWebhookBot {
    String botPath;
    String botUsername;
    String botToken;

    MessageHandler messageHandler;
    private BotService botService;

    public Bot(SetWebhook setWebhook, MessageHandler messageHandler) {
        super(setWebhook);
        this.messageHandler = messageHandler;
    }

    @Override
    public String getBotUsername() {
        return "BeltyukovDiplomBot";
    }

    @Override
    public String getBotToken() {
        return "5583700436:AAFsBJOGeAVxY2BzlmZ85f7Kq8I6ZJYhXhM";
    }

    @Override
    public BotApiMethod<?> onWebhookUpdateReceived(Update update) {
        try {
            return handleUpdate(update);
        } catch (Exception e) {
            return new SendMessage(
                    update.getMessage().getChatId().toString(),
                    e.getMessage());
        }
    }

    private BotApiMethod<?> handleUpdate(Update update) {
        return messageHandler.answerMessage(update.getMessage());
    }
}
