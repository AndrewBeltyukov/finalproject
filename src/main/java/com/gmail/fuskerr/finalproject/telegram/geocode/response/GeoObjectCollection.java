package com.gmail.fuskerr.finalproject.telegram.geocode.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class GeoObjectCollection {
    public ArrayList<FeatureMember> featureMember;
}
