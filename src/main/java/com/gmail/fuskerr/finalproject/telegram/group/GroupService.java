package com.gmail.fuskerr.finalproject.telegram.group;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.nodes.Tag;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GroupService {
    private final GroupRepository groupRepository;

    public void create(Group group) {
        groupRepository.save(group);
    }

    public void update(Group group) {
        groupRepository.update(group);
    }

    public Optional<Group> getByUserId(Long user) {
        return groupRepository.findByUserId(user);
    }

    public Optional<TelegramTag> getTagByName(String name) {
        return groupRepository.findTagByName(name);
    }

    public void deleteByUserId(Long id) {
        groupRepository.deleteById(id);
    }

    public List<Group> getByTagAndDescription(TelegramTag tag, String description) {
        return groupRepository.findByTagAndDescriptionLike(tag.getId(), description);
    }
}
