package com.gmail.fuskerr.finalproject.telegram.auth.repository;

import com.gmail.fuskerr.finalproject.telegram.auth.model.TelegramUser;
import com.gmail.fuskerr.finalproject.telegram.auth.model.TelegramUserRole;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Mapper
public interface TelegramUserRepository {

    @Results(id = "selectTelegramUser", value = {
            @Result(property = "id", column = "id", javaType = Long.class),
            @Result(property = "name", column = "name"),
            @Result(property = "email", column = "email"),
            @Result(property = "sex", column = "sex"),
            @Result(property = "age", column = "age"),
            @Result(
                    property = "roles",
                    column = "id",
                    javaType = Set.class,
                    many = @Many(select = "getTelegramRoles")
            )
    })
    @Select("SELECT id, name, email, sex, age FROM TELEGRAM_USER WHERE name = #{name}")
    Optional<TelegramUser> findByUsername(@Param("name") String name);

    @Select("SELECT role.name FROM TELEGRAM_ROLE role " +
            "JOIN TELEGRAM_USER_ROLE_RELATION middle " +
            "ON role.id = middle.role_id " +
            "WHERE middle.user_id = #{user_id}")
    List<TelegramUserRole> getTelegramRoles(@Param("user_id") Long id);

    @ResultMap("selectTelegramUser")
    @Select("SELECT * FROM TELEGRAM_USER u " +
            "JOIN TELEGRAM_USER_GROUP_RELATION middle " +
            "ON u.id = middle.user_id " +
            "WHERE middle.group_id = #{group_id}")
    List<TelegramUser> getUsersByGroupId(@Param("group_id") Long id);

    @ResultMap("selectTelegramUser")
    @Select("SELECT id, name, email, sex, age FROM TELEGRAM_USER WHERE id = #{id}")
    Optional<TelegramUser> findById(@Param("id") Long id);

    @Insert("INSERT INTO TELEGRAM_USER(name, email, sex, age) " +
            "VALUES(#{user.name}, #{user.email}, #{user.sex}, #{user.age})")
    @Options(useGeneratedKeys = true, keyProperty = "user.id")
    void save(@Param("user") TelegramUser user);

    @Insert({"<script>",
            "INSERT INTO TELEGRAM_USER_ROLE_RELATION(user_id, role_id) ",
            "VALUES" +
                    "<foreach collection='roles' item='role'  open='' separator=',' close=''>",
            "(#{user_id}, (SELECT r.id FROM TELEGRAM_ROLE r WHERE r.name = #{role.name})) ",
            "</foreach>",
            "</script>"})
    void saveRolesToUser(@Param("user_id") Long userId, @Param("roles") Set<TelegramUserRole> roles);

    @Update("UPDATE TELEGRAM_USER SET " +
            "name=#{name}, email=#{email}, sex=#{sex}, age=#{age} " +
            "WHERE id =#{id}")
    Long update(TelegramUser user);
}
