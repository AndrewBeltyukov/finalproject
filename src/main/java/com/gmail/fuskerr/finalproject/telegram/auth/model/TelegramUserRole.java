package com.gmail.fuskerr.finalproject.telegram.auth.model;

public record TelegramUserRole(
        Long id,
        String name) {
}
