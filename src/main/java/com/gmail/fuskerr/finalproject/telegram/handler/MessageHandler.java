package com.gmail.fuskerr.finalproject.telegram.handler;

import com.gmail.fuskerr.finalproject.telegram.BotMessageEnum;
import com.gmail.fuskerr.finalproject.telegram.auth.TelegramUserService;
import com.gmail.fuskerr.finalproject.telegram.auth.model.TelegramUser;
import com.gmail.fuskerr.finalproject.telegram.auth.repository.TelegramUserRepository;
import com.gmail.fuskerr.finalproject.telegram.button.ButtonNameEnum;
import com.gmail.fuskerr.finalproject.telegram.button.ReplyKeyboardMaker;
import com.gmail.fuskerr.finalproject.telegram.geocode.YandexService;
import com.gmail.fuskerr.finalproject.telegram.group.Group;
import com.gmail.fuskerr.finalproject.telegram.group.GroupRepository;
import com.gmail.fuskerr.finalproject.telegram.group.GroupService;
import com.gmail.fuskerr.finalproject.telegram.group.TelegramTag;
import lombok.RequiredArgsConstructor;
import org.flywaydb.core.internal.util.Pair;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class MessageHandler {
    private final ReplyKeyboardMaker replyKeyboardMaker;

    private final TelegramUserService telegramUserService;
    private final GroupService groupService;
    private final YandexService yandexService;

    public BotApiMethod<?> answerMessage(Message message) {
        String chatId = message.getChatId().toString();
        String inputText = message.getText();
        ButtonNameEnum buttonName = ButtonNameEnum.getByName(inputText);
        String username = message.getFrom().getUserName();

        //Проверяем пользователя
        Optional<TelegramUser> optionalTelegramUser = telegramUserService.getUserByName(username);
        if(optionalTelegramUser.isEmpty()) {
            return registerUser(username, chatId);
        }
        TelegramUser user = optionalTelegramUser.get();
        if(user.getSex() == null || user.getEmail() == null || user.getAge() == null) {
            return fillAllFiledOfUser(user, chatId, inputText);
        }

        //Отвечаем на сообщение
        return switch (buttonName) {
            case START_BUTTON, CANCEL_BUTTON -> createMessageWithMainMenu(chatId, BotMessageEnum.START_MESSAGE.getMessage());
            case GROUP_NAME_COMMAND -> fillGroupName(user, chatId, inputText);
            case GROUP_ADDRESS_COMMAND ->fillGroupAddress(user, chatId, inputText);
            case GROUP_DESCRIPTION_COMMAND ->fillGroupDescription(user, chatId, inputText);
            case GROUP_TAG_COMMAND ->fillGroupTag(user, chatId, inputText);
            case GROUP_TIME_COMMAND ->fillGroupTime(user, chatId, inputText);
            case FIND_COMMAND -> findGroup(user, chatId, inputText);

            case CREATE_GROUP_BUTTON -> createEmptyGroup(user, chatId, inputText);
            case DELETE_GROUP_BUTTON -> deleteGroup(user, chatId, inputText);
            case FIND_GROUP_BUTTON -> createMessageWithMainMenu(chatId, BotMessageEnum.FIND_MESSAGE.getMessage());
            default -> createMessageWithoutMenu(chatId, BotMessageEnum.EXCEPTION_UNKNOWN.getMessage());
        };
    }

    private SendMessage findGroup(TelegramUser user, String chatId, String text) {
        String[] strings = text.split("\\s+");
        String tag = strings[1];
        String description = "";
        if(strings.length > 2) {
            description = strings[2];
        }
        Optional<TelegramTag> tagFromDB = groupService.getTagByName(tag);
        if(tagFromDB.isEmpty()) {
            return createMessageWithMainMenu(chatId, BotMessageEnum.TAG_MESSAGE.getMessage());
        }
        List<Group> groups = groupService.getByTagAndDescription(tagFromDB.get(), description);
        StringBuilder message = new StringBuilder();
        if(!groups.isEmpty()) {
            for(Group group : groups) {
                message.append(group.toString());
                message.append("\n");
            }
        } else {
            return createMessageWithMainMenu(chatId, BotMessageEnum.GROUP_NOT_FOUND.getMessage());
        }
        return createMessageWithMainMenu(chatId, message.toString());
    }

    private SendMessage deleteGroup(TelegramUser user, String chatId, String text) {
        groupService.deleteByUserId(user.getId());
        return createMessageWithMainMenu(chatId, BotMessageEnum.DELETED_GROUP.getMessage());
    }

    private SendMessage createEmptyGroup(TelegramUser user, String chatId, String text) {
        Optional<Group> optionalGroup = groupService.getByUserId(user.getId());
        Group group;
        if(optionalGroup.isEmpty()) {
            group = new Group(user);
            groupService.create(group);
        } else {
            group = optionalGroup.get();
            return createMessageWithDeleteCancel(chatId, group.toString());
        }
        return createMessageWithDeleteCancel(chatId, BotMessageEnum.CREATE_GROUP_MESSAGE.getMessage());
    }

    private SendMessage fillGroupName(TelegramUser user, String chatId, String text) {
        Optional<Group> optionalGroup = groupService.getByUserId(user.getId());
        if(optionalGroup.isEmpty()) {
            return createMessageWithDeleteCancel(chatId, BotMessageEnum.EXCEPTION_UNKNOWN.getMessage());
        }
        Group group = optionalGroup.get();
        group.setName(text.split(ButtonNameEnum.GROUP_NAME_COMMAND.getButtonName() + " ")[1]);
        groupService.update(group);
        return createMessageWithDeleteCancel(chatId, group.toString());
    }

    private SendMessage fillGroupAddress(TelegramUser user, String chatId, String text) {
        Optional<Group> optionalGroup = groupService.getByUserId(user.getId());
        if(optionalGroup.isEmpty()) {
            return createMessageWithDeleteCancel(chatId, BotMessageEnum.EXCEPTION_UNKNOWN.getMessage());
        }
        Group group = optionalGroup.get();
        String address = text.split(ButtonNameEnum.GROUP_ADDRESS_COMMAND.getButtonName() + " ")[1];
        group.setAddress(address);

        Pair<String, String> latLongPair = yandexService.getLatLongByAddress(address);
        group.setLatitude(latLongPair.getLeft());
        group.setLongitude(latLongPair.getRight());

        groupService.update(group);
        return createMessageWithDeleteCancel(chatId, group.toString());
    }

    private SendMessage fillGroupDescription(TelegramUser user, String chatId, String text) {
        Optional<Group> optionalGroup = groupService.getByUserId(user.getId());
        if(optionalGroup.isEmpty()) {
            return createMessageWithDeleteCancel(chatId, BotMessageEnum.EXCEPTION_UNKNOWN.getMessage());
        }
        Group group = optionalGroup.get();
        group.setDescription(text.split(ButtonNameEnum.GROUP_DESCRIPTION_COMMAND.getButtonName() + " ")[1]);
        groupService.update(group);
        return createMessageWithDeleteCancel(chatId, group.toString());
    }

    private SendMessage fillGroupTag(TelegramUser user, String chatId, String text) {
        Optional<Group> optionalGroup = groupService.getByUserId(user.getId());
        if(optionalGroup.isEmpty()) {
            return createMessageWithDeleteCancel(chatId, BotMessageEnum.EXCEPTION_UNKNOWN.getMessage());
        }
        Group group = optionalGroup.get();
        String tagName = text.split("\\s+")[1];
        Optional<TelegramTag> optionalTelegramTag = groupService.getTagByName(tagName);
        if(optionalTelegramTag.isEmpty()) {
            return createMessageWithDeleteCancel(chatId, BotMessageEnum.TAG_MESSAGE.getMessage());
        }
        TelegramTag tag = optionalTelegramTag.get();
        group.setTag(tag);
        groupService.update(group);
        return createMessageWithDeleteCancel(chatId, group.toString());
    }

    private SendMessage fillGroupTime(TelegramUser user, String chatId, String text) {
        Optional<Group> optionalGroup = groupService.getByUserId(user.getId());
        if(optionalGroup.isEmpty()) {
            return createMessageWithDeleteCancel(chatId, BotMessageEnum.EXCEPTION_UNKNOWN.getMessage());
        }
        Group group = optionalGroup.get();

        String[] strings = text.split("\\s+");
        String date = "";
        try {
            date = strings[1] + " " + strings[2];
        } catch (Exception e) {
            return createMessageWithDeleteCancel(chatId, "Время передается в фаормате yyyy-MM-dd HH:mm");
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter);

        group.setTime(dateTime);
        groupService.update(group);
        return createMessageWithDeleteCancel(chatId, group.toString());
    }

    private SendMessage createMessageWithoutMenu(String chatId, String message) {
        SendMessage sendMessage = new SendMessage(chatId, message);
        sendMessage.setReplyMarkup(null);
        return sendMessage;
    }

    private SendMessage createMessageWithDeleteCancel(String chatId, String message) {
        SendMessage sendMessage = new SendMessage(chatId, message);
        sendMessage.setReplyMarkup(replyKeyboardMaker.getDeleteCancelKeyboard());
        return sendMessage;
    }

    private SendMessage createMessageWithMainMenu(String chatId, String message) {
        SendMessage sendMessage = new SendMessage(chatId, message);
        sendMessage.setReplyMarkup(replyKeyboardMaker.getMainMenuKeyboard());
        return sendMessage;
    }

    private SendMessage registerUser(String username, String chatId) {
        TelegramUser user = new TelegramUser(username);
        telegramUserService.saveUser(user);

        return createMessageWithoutMenu(chatId, BotMessageEnum.EMAIL_NEED_MESSAGE.getMessage());
    }

    private SendMessage fillAllFiledOfUser(TelegramUser user, String chatId, String inputText) {
        if(user.getEmail() == null) {
            if(inputText.contains(ButtonNameEnum.EMAIL_COMMAND.getButtonName())) {
                String email = inputText.split("\\s+")[1];
                user.setEmail(email);
                telegramUserService.update(user);
                return createMessageWithoutMenu(chatId, BotMessageEnum.AGE_NEED_MESSAGE.getMessage());
            } else {
                return createMessageWithoutMenu(chatId, BotMessageEnum.EMAIL_NEED_MESSAGE.getMessage());
            }
        } else if(user.getAge() == null) {
            if(inputText.contains(ButtonNameEnum.AGE_COMMAND.getButtonName())) {
                Integer age = Integer.valueOf(inputText.split("\\s+")[1]);
                user.setAge(age);
                telegramUserService.update(user);
                return createMessageWithoutMenu(chatId, BotMessageEnum.SEX_NEED_MESSAGE.getMessage());
            } else {
                return createMessageWithoutMenu(chatId, BotMessageEnum.AGE_NEED_MESSAGE.getMessage());
            }
        } else if(user.getSex() == null) {
            if(inputText.contains(ButtonNameEnum.SEX_COMMAND.getButtonName())) {
                String sex = inputText.split("\\s+")[1];
                user.setSex(sex);
                telegramUserService.update(user);
                return createMessageWithMainMenu(chatId, BotMessageEnum.START_MESSAGE.getMessage());
            } else {
                return createMessageWithoutMenu(chatId, BotMessageEnum.SEX_NEED_MESSAGE.getMessage());
            }
        }
        return createMessageWithoutMenu(chatId, BotMessageEnum.EXCEPTION_UNKNOWN.getMessage());
    }
}
