package com.gmail.fuskerr.finalproject.telegram;

public enum BotMessageEnum {
    //ответы на команды с клавиатуры
    START_MESSAGE("Вы авторизованы. Воспользуйтесь клавиатурой, чтобы продолжить работу\uD83D\uDC47"),
    EMAIL_NEED_MESSAGE("Нам нужна ваша почта для продолжения использования.\nНапример /email mail@mail.com"),
    AGE_NEED_MESSAGE("Нам нужен ваш возраст для продолжения.\nНапример /age 23"),
    SEX_NEED_MESSAGE("Нам нужен ваш пол(М или Ж), чтобы продолжить.\nНапример /sex М"),
    EXCEPTION_UNKNOWN("Что-то пошло не так. Обратитесь к программисту."),

    CREATE_GROUP_MESSAGE("""
            Для создания группы нужно заполнить поля:
            /name Название группы
            /address Ижевск, ул. Удмурская, 250
            /desc Описание: Бег по утрам
            /tag Тег: Отдых, Спорт, Развлечение
            /time 2022-06-30 08:00"""),
    TAG_MESSAGE("Нужно выбрать один из тегов: Отдых, Спорт, Развлечение"),
    DELETED_GROUP("Группа успешно удалена"),
    FIND_MESSAGE("Для поиска группы передайте тег(Отдых, Спорт, Развлечение) и ключевое слово из описания." +
            "\nНапример: /find Спорт бег"),
    GROUP_NOT_FOUND("К сожалению, таких групп у нас не оказалось. Попробуйте другие запросы или создайте свою группу.");

    private final String message;

    BotMessageEnum(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
