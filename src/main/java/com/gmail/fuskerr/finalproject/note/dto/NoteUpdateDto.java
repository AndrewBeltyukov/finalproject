package com.gmail.fuskerr.finalproject.note.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public record NoteUpdateDto(
        @NotNull
        Long id,
        @NotBlank
        String title,
        @NotBlank
        String text,
        @NotBlank
        String tag,
        Long noteReference) {
}
