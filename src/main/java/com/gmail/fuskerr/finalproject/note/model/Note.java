package com.gmail.fuskerr.finalproject.note.model;

import com.gmail.fuskerr.finalproject.model.BaseForm;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;


@NoArgsConstructor
@Getter
@ToString(callSuper = true)
public class Note extends BaseForm {
    private Long noteReference;

    public Note(
            String title,
            String text,
            String tag,
            Long noteReference,
            LocalDateTime createdOn,
            LocalDateTime lastModify) {
        super(null, title, text, tag, createdOn, lastModify);
        this.noteReference = noteReference;
    }

    public Note(
            Long id,
            String title,
            String text,
            String tag,
            Long noteReference,
            LocalDateTime createdOn,
            LocalDateTime lastModify) {
        super(id, title, text, tag, createdOn, lastModify);
        this.noteReference = noteReference;
    }
}
