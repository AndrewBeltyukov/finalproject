package com.gmail.fuskerr.finalproject.note.repository;

import com.github.pagehelper.Page;
import com.gmail.fuskerr.finalproject.note.model.Note;
import org.apache.ibatis.annotations.*;

import java.util.Optional;

@Mapper
public interface NoteRepository {
    @Results(id = "selectNote", value = {
            @Result(property = "id", column = "id", id = true),
            @Result(property = "title", column = "title"),
            @Result(property = "text", column = "text"),
            @Result(property = "tag", column = "tag"),
            @Result(property = "noteReference", column = "note_reference"),
            @Result(property = "createdOn", column = "created_on"),
            @Result(property = "lastModify", column = "last_modify")
    })
    @Select("SELECT id, title, text, tag, note_reference, created_on, last_modify " +
            "FROM NOTE WHERE id = #{note_id}")
    Optional<Note> findById(@Param("note_id") Long noteId);

    @ResultMap("selectNote")
    @Select("SELECT id, title, text, tag, note_reference, created_on, last_modify " +
            "FROM NOTE WHERE tag = #{tag}")
    Page<Note> findByTag(@Param("tag") String tag);

    @Insert("INSERT INTO NOTE(title, text, tag, note_reference, created_on, last_modify) " +
            "VALUES(#{title}, #{text}, #{tag}, #{noteReference}, now(), now())")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void save(Note note);

    @Update("UPDATE NOTE SET " +
            "title=#{title}, text=#{text}, tag=#{tag}, note_reference=#{noteReference}, last_modify=now() " +
            "WHERE id =#{id}")
    Long update(Note note);

    @ResultMap("selectNote")
    @Select("SELECT id, title, text, tag, note_reference, created_on, last_modify " +
            "FROM NOTE")
    Page<Note> findAll();

    @Update("UPDATE NOTE SET " +
            "note_reference=NULL, last_modify=now() " +
            "WHERE note_reference=#{note_id}")
    Long setNullReferencesById(@Param("note_id") Long noteId);

    @Delete("DELETE FROM NOTE WHERE id = #{note_id}")
    Long deleteById(@Param("note_id") Long id);
}
