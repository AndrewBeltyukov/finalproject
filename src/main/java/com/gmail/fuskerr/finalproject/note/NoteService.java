package com.gmail.fuskerr.finalproject.note;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.gmail.fuskerr.finalproject.note.model.Note;
import com.gmail.fuskerr.finalproject.note.repository.NoteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class NoteService {
    private final NoteRepository repository;

    public Note createAndGet(Note note) {
        // проверить, что заметка из ссылки существует
        checkNoteReferenceExists(note);
        repository.save(note);
        return getById(note.getId());
    }

    public Note getById(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new NoteNotFoundException("Not found note with id: " + id));
    }

    public Page<Note> getByPage(Integer page, Integer size) {
        PageHelper.startPage(page, size);
        return repository.findAll();
    }

    public Page<Note> getByTagPage(String tag, Integer page, Integer size) {
        PageHelper.startPage(page, size);
        return repository.findByTag(tag);
    }

    public boolean update(Note note) {
        // проверить, что заметка из ссылки существует
        checkNoteReferenceExists(note);
        Long changedRows = repository.update(note);
        return isChanged(changedRows);
    }

    public boolean deleteById(Long id) {
        repository.setNullReferencesById(id);
        Long changedRows = repository.deleteById(id);
        return isChanged(changedRows);
    }

    private boolean isChanged(Long rowCount) {
        return rowCount > 0;
    }

    // проверить, что заметка из ссылки существует
    private void checkNoteReferenceExists(Note note) {
        Long noteReferenceId = note.getNoteReference();
        // если у заметки есть ссылка на другую заметку, то проверить, что она существует
        if (noteReferenceId != null) {
            getById(noteReferenceId);
        }
    }
}
