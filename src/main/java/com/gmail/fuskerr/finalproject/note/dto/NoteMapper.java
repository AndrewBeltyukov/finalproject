package com.gmail.fuskerr.finalproject.note.dto;

import com.github.pagehelper.Page;
import com.gmail.fuskerr.finalproject.note.model.Note;

import java.util.List;
import java.util.stream.Collectors;

public class NoteMapper {

    public static Note convertFromDto(NoteCreateDto dto) {
        return new Note(
                dto.title(),
                dto.text(),
                dto.tag(),
                dto.noteReference(),
                null,
                null
        );
    }

    public static Note convertFromDto(NoteUpdateDto dto) {
        return new Note(
                dto.id(),
                dto.title(),
                dto.text(),
                dto.tag(),
                dto.noteReference(),
                null,
                null
        );
    }

    public static NoteResponseDto convertToDto(Note note) {
        return new NoteResponseDto(
                note.getId(),
                note.getTitle(),
                note.getText(),
                note.getTag(),
                note.getNoteReference(),
                note.getCreatedOn(),
                note.getLastModify()
        );
    }

    public static Page<NoteResponseDto> convertToDto(Page<Note> notePage) {
        List<Note> notes = notePage.getResult();
        List<NoteResponseDto> gets = notes.stream().map(NoteMapper::convertToDto).collect(Collectors.toList());
        Page<NoteResponseDto> getPage = new Page<>(notePage.getPageNum(), notePage.getPageSize(), notePage.isCount());
        getPage.addAll(gets);
        return getPage;
    }
}
