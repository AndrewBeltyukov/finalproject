package com.gmail.fuskerr.finalproject.note;

import com.github.pagehelper.Page;
import com.gmail.fuskerr.finalproject.dto.SuccessDto;
import com.gmail.fuskerr.finalproject.note.dto.NoteCreateDto;
import com.gmail.fuskerr.finalproject.note.dto.NoteMapper;
import com.gmail.fuskerr.finalproject.note.dto.NoteResponseDto;
import com.gmail.fuskerr.finalproject.note.dto.NoteUpdateDto;
import com.gmail.fuskerr.finalproject.note.model.Note;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/note")
@RequiredArgsConstructor
public class NoteController {
    private final NoteService noteService;

    @PostMapping
    public NoteResponseDto create(
            @Valid
            @RequestBody NoteCreateDto noteDto
    ) {
        Note note = NoteMapper.convertFromDto(noteDto);
        Note createdNote = noteService.createAndGet(note);
        return NoteMapper.convertToDto(createdNote);
    }

    @GetMapping("/{id}")
    public NoteResponseDto get(@PathVariable("id") Long id) {
        Note note = noteService.getById(id);
        return NoteMapper.convertToDto(note);
    }

    @GetMapping
    public Page<NoteResponseDto> getPage(
            @RequestParam(name = "page", defaultValue = "0") Integer page,
            @RequestParam(name = "size", defaultValue = "5") Integer size) {
        Page<Note> notePage = noteService.getByPage(page, size);
        return NoteMapper.convertToDto(notePage);
    }

    @GetMapping("/tag/{tag}")
    public Page<NoteResponseDto> getPage(
            @RequestParam(name = "page", defaultValue = "0") Integer page,
            @RequestParam(name = "size", defaultValue = "5") Integer size,
            @PathVariable("tag") String tag) {
        return NoteMapper.convertToDto(noteService.getByTagPage(tag, page, size));
    }

    @PutMapping
    public SuccessDto update(
            @Valid
            @RequestBody NoteUpdateDto noteDto) {
        boolean success = noteService.update(NoteMapper.convertFromDto(noteDto));
        return new SuccessDto(success);
    }

    @DeleteMapping("/{id}")
    public SuccessDto delete(@PathVariable("id") Long id) {
        boolean success = noteService.deleteById(id);
        return new SuccessDto(success);
    }
}
