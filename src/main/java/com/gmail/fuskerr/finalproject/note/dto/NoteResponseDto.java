package com.gmail.fuskerr.finalproject.note.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public record NoteResponseDto(
        Long id,
        String title,
        String text,
        String tag,
        Long noteReference,
        @JsonFormat(pattern = "yyyy.MM.dd HH:mm:ss")
        LocalDateTime createdOn,
        @JsonFormat(pattern = "yyyy.MM.dd HH:mm:ss")
        LocalDateTime lastModify) {
}
