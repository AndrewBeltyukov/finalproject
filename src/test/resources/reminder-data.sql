ALTER SEQUENCE event_reminder_id_seq RESTART WITH 1;

INSERT INTO EVENT_REMINDER(title, text, tag, created_on, last_modify, creator, date)
VALUES('title1', 'text1', 'tag', '2022.01.01 00:00:00', '2022.01.01 00:00:00', 1, '2022.01.01 00:00:00');

INSERT INTO EVENT_REMINDER(title, text, tag, created_on, last_modify, creator, date)
VALUES('title2', 'text2', 'tag', '2022.01.01 00:00:00', '2022.01.01 00:00:00', 1, '2022.01.01 00:00:00');

INSERT INTO EVENT_REMINDER(title, text, tag, created_on, last_modify, creator, date)
VALUES('title3', 'text3', 'tag', '2022.01.01 00:00:00', '2022.01.01 00:00:00', 1, '2022.01.01 00:00:00');

INSERT INTO EVENT_REMINDER(title, text, tag, created_on, last_modify, creator, date)
VALUES('title4', 'text4', 'tag', '2022.01.01 00:00:00', '2022.01.01 00:00:00', 1, '2022.01.01 00:00:00');