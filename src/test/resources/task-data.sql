ALTER SEQUENCE task_id_seq RESTART WITH 1;

INSERT INTO TASK(title, text, tag, status_name, assigned, board_id, created_on, last_modify)
VALUES('title1', 'text1', 'task', 'TEST', null, 1, '2022.01.01 00:00:00', '2022.01.01 00:00:00');
INSERT INTO TASK(title, text, tag, status_name, assigned, board_id, created_on, last_modify)
VALUES('title2', 'text2', 'task', 'TEST', null, 1, '2022.01.01 00:00:00', '2022.01.01 00:00:00');
INSERT INTO TASK(title, text, tag, status_name, assigned, board_id, created_on, last_modify)
VALUES('title3', 'text3', 'task', 'TEST', null, 2, '2022.01.01 00:00:00', '2022.01.01 00:00:00');
INSERT INTO TASK(title, text, tag, status_name, assigned, board_id, created_on, last_modify)
VALUES('title4', 'text4', 'task', 'TEST', null, 2, '2022.01.01 00:00:00', '2022.01.01 00:00:00');