ALTER SEQUENCE note_id_seq RESTART WITH 1;

INSERT INTO NOTE(title, text, tag, note_reference, created_on, last_modify)
VALUES('title1', 'text1', 'tag', null, '2022.01.01 00:00:00', '2022.01.01 00:00:00');

INSERT INTO NOTE(title, text, tag, note_reference, created_on, last_modify)
VALUES('title2', 'text2', 'tag', null, '2022.01.01 00:00:00', '2022.01.01 00:00:00');

INSERT INTO NOTE(title, text, tag, note_reference, created_on, last_modify)
VALUES('title3', 'text3', 'tag', null, '2022.01.01 00:00:00', '2022.01.01 00:00:00');

INSERT INTO NOTE(title, text, tag, note_reference, created_on, last_modify)
VALUES('title4', 'text4', 'tag', null, '2022.01.01 00:00:00', '2022.01.01 00:00:00');