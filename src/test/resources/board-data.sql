ALTER SEQUENCE board_id_seq RESTART WITH 1;

INSERT INTO BOARD(title, text, tag, created_on, last_modify) VALUES('title1', 'text1', 'tag', '2022.01.01 00:00:00', '2022.01.01 00:00:00');
INSERT INTO BOARD(title, text, tag, created_on, last_modify) VALUES('title2', 'text2', 'tag', '2022.01.01 00:00:00', '2022.01.01 00:00:00');
INSERT INTO BOARD(title, text, tag, created_on, last_modify) VALUES('title3', 'text3', 'tag1', '2022.01.01 00:00:00', '2022.01.01 00:00:00');
INSERT INTO BOARD(title, text, tag, created_on, last_modify) VALUES('title4', 'text4', 'tag1', '2022.01.01 00:00:00', '2022.01.01 00:00:00');
INSERT INTO BOARD(title, text, tag, created_on, last_modify) VALUES('title5', 'text5', 'tag1', '2022.01.01 00:00:00', '2022.01.01 00:00:00');
INSERT INTO BOARD(title, text, tag, created_on, last_modify) VALUES('title6', 'text6', 'tag1', '2022.01.01 00:00:00', '2022.01.01 00:00:00');