package com.gmail.fuskerr.finalproject.note;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gmail.fuskerr.finalproject.ApplicationTests;
import com.gmail.fuskerr.finalproject.board.dto.BoardResponseDto;
import com.gmail.fuskerr.finalproject.board.dto.BoardUpdateDto;
import com.gmail.fuskerr.finalproject.board.status.dto.TaskStatusDto;
import com.gmail.fuskerr.finalproject.board.task.dto.TaskCreateDto;
import com.gmail.fuskerr.finalproject.board.task.dto.TaskResponseDto;
import com.gmail.fuskerr.finalproject.note.dto.NoteCreateDto;
import com.gmail.fuskerr.finalproject.note.dto.NoteResponseDto;
import com.gmail.fuskerr.finalproject.note.dto.NoteUpdateDto;
import com.gmail.fuskerr.finalproject.reminder.notification.NotificationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@Transactional
@Sql(scripts = "classpath:note-data.sql")
class NoteControllerTest extends ApplicationTests {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    NotificationService notificationService;

    private final String baseUrl = "/note/";

    private final String username = "admin@email.com";
    private final String password = "password";
    private final String createdOnField = "createdOn";
    private final String lastModifyField = "lastModify";

    @Test
    void createNoteAndGet_ShouldReturnCreatedNote() throws Exception {
        Long id = 5L;
        String title = "title";
        String text = "text";
        String tag = "task";

        NoteCreateDto createDto = new NoteCreateDto(title, text, tag, null);
        String createJson = objectMapper.writeValueAsString(createDto);
        NoteResponseDto expected = new NoteResponseDto(id, title, text, tag, null, null, null);

        String actualJson = mockMvc.perform(post(baseUrl)
                        .with(httpBasic(username, password))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(createJson))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        NoteResponseDto actual = objectMapper.readValue(actualJson, NoteResponseDto.class);

        assertThat(actual)
                .usingRecursiveComparison()
                .ignoringFields(createdOnField, lastModifyField)
                .isEqualTo(expected);
    }

    @Test
    void getNoteWithId1_ShouldReturnNoteWithId1() throws Exception {
        Long id = 1L;
        String title = "title1";
        String text = "text1";
        String tag = "tag";
        LocalDateTime date = LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0, 0);
        NoteResponseDto noteResponseDto = new NoteResponseDto(id, title, text, tag, null, date, date);
        String expected = objectMapper.writeValueAsString(noteResponseDto);

        mockMvc.perform(get(baseUrl + id)
                        .with(httpBasic(username, password)))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
    }

    @Test
    void getNotesWithPage2Size2_ShouldReturn2Notes() throws Exception {
        Long id3 = 3L;
        Long id4 = 4L;
        String title3 = "title3";
        String title4 = "title4";
        String text3 = "text3";
        String text4 = "text4";
        LocalDateTime date = LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0, 0);
        String tag = "tag";
        List<NoteResponseDto> noteResponses = List.of(
                new NoteResponseDto(id3, title3, text3, tag, null, date, date),
                new NoteResponseDto(id4, title4, text4, tag, null, date, date)
        );
        String expected = objectMapper.writeValueAsString(noteResponses);
        mockMvc.perform(get(baseUrl + "?page=2&size=2")
                        .with(httpBasic(username, password)))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
    }

    @Test
    void getNotesPage2Size2WithTag_ShouldReturn2NoteWithTag() throws Exception {
        Long id3 = 3L;
        Long id4 = 4L;
        String title3 = "title3";
        String title4 = "title4";
        String text3 = "text3";
        String text4 = "text4";
        LocalDateTime date = LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0, 0);
        String tag = "tag";
        List<NoteResponseDto> noteResponses = List.of(
                new NoteResponseDto(id3, title3, text3, tag, null, date, date),
                new NoteResponseDto(id4, title4, text4, tag, null, date, date)
        );
        String expected = objectMapper.writeValueAsString(noteResponses);
        mockMvc.perform(get(baseUrl + "tag/" + tag + "?page=2&size=2")
                        .with(httpBasic(username, password)))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
    }

    @Test
    void updateNoteAndGet_ShouldReturnUpdatedNote() throws Exception {
        Long id = 1L;
        Long referenceId = 3L;
        String title = "new title";
        String text = "new text";
        String tag = "new tag";
        NoteUpdateDto noteUpdateDto = new NoteUpdateDto(id, title, text, tag, referenceId);
        String noteUpdateDtoJson = objectMapper.writeValueAsString(noteUpdateDto);
        mockMvc.perform(put(baseUrl).with(httpBasic(username, password))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(noteUpdateDtoJson))
                .andExpect(status().isOk());

        NoteResponseDto expected = new NoteResponseDto(id, title, text, tag, referenceId, null, null);

        String actualJson = mockMvc.perform(get(baseUrl + id).with(httpBasic(username, password)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        NoteResponseDto actual = objectMapper.readValue(actualJson, NoteResponseDto.class);

        assertThat(actual)
                .usingRecursiveComparison()
                .ignoringFields(createdOnField, lastModifyField)
                .isEqualTo(expected);
    }

    @Test
    void deleteNoteAndGet_ShouldReturnNotFoundError() throws Exception {
        Long id = 1L;
        mockMvc.perform(delete(baseUrl + id).with(httpBasic(username, password)))
                .andExpect(status().isOk());
        mockMvc.perform(get(baseUrl + id).with(httpBasic(username, password)))
                .andExpect(status().isNotFound());
    }
}