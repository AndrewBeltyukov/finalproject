package com.gmail.fuskerr.finalproject;

import com.gmail.fuskerr.finalproject.init.Postgres;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
@SpringBootTest
@ContextConfiguration(initializers = Postgres.Initializer.class)
public class ApplicationTests {
    @BeforeAll
    static void init() {
        Postgres.container.start();
    }
}
