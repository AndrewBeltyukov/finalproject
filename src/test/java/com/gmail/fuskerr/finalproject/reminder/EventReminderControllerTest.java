package com.gmail.fuskerr.finalproject.reminder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gmail.fuskerr.finalproject.ApplicationTests;
import com.gmail.fuskerr.finalproject.auth.dto.UserResponseDto;
import com.gmail.fuskerr.finalproject.reminder.dto.EventReminderCreateDto;
import com.gmail.fuskerr.finalproject.reminder.dto.EventReminderResponseDto;
import com.gmail.fuskerr.finalproject.reminder.dto.EventReminderUpdateDto;
import com.gmail.fuskerr.finalproject.reminder.notification.NotificationService;
import com.gmail.fuskerr.finalproject.reminder.scheduling.TaskSchedulingService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@Transactional
@Sql(scripts = "classpath:reminder-data.sql")
class EventReminderControllerTest extends ApplicationTests {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    NotificationService notificationService;
    @MockBean
    TaskSchedulingService taskSchedulingService;

    private final String baseUrl = "/reminder/";

    private final String username = "admin@email.com";
    private final String password = "password";
    private final String createdOnField = "createdOn";
    private final String lastModifyField = "lastModify";

    @Test
    void createAndGet_ShouldReturnCreatedReminder() throws Exception {
        Long id = 5L;
        String title = "title";
        String text = "text";
        String tag = "task";
        LocalDateTime date = LocalDateTime.now();

        EventReminderCreateDto createDto = new EventReminderCreateDto(title, text, tag, date);
        String createJson = objectMapper.writeValueAsString(createDto);
        UserResponseDto creator = new UserResponseDto(1L, "admin", username);
        EventReminderResponseDto expected = new EventReminderResponseDto(id, title, text, tag, date.withNano(0), creator, Collections.emptyList(), null, null);

        String actualJson = mockMvc.perform(post(baseUrl)
                        .with(httpBasic(username, password))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(createJson))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        EventReminderResponseDto actual = objectMapper.readValue(actualJson, EventReminderResponseDto.class);

        assertThat(actual)
                .usingRecursiveComparison()
                .ignoringFields(createdOnField, lastModifyField)
                .isEqualTo(expected);
    }

    @Test
    void getReminderWithId1_ShouldReturnReminderWithId1() throws Exception {
        Long id = 1L;
        String title = "title1";
        String text = "text1";
        String tag = "tag";
        UserResponseDto creator = new UserResponseDto(1L, "admin", username);
        LocalDateTime date = LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0, 0);
        EventReminderResponseDto reminderResponseDto = new EventReminderResponseDto(id, title, text, tag, date, creator, Collections.emptyList(), date, date);
        String expected = objectMapper.writeValueAsString(reminderResponseDto);

        mockMvc.perform(get(baseUrl + id)
                        .with(httpBasic(username, password)))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
    }

    @Test
    void getAllReminders_ShouldReturnListOfAllReminders() throws Exception {
        LocalDateTime date = LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0, 0);
        String tag = "tag";
        UserResponseDto creator = new UserResponseDto(1L, "admin", username);
        List<EventReminderResponseDto> reminderResponses = List.of(
                new EventReminderResponseDto(1L, "title1", "text1", tag, date, creator, Collections.emptyList(), date, date),
                new EventReminderResponseDto(2L, "title2", "text2", tag, date, creator, Collections.emptyList(), date, date),
                new EventReminderResponseDto(3L, "title3", "text3", tag, date, creator, Collections.emptyList(), date, date),
                new EventReminderResponseDto(4L, "title4", "text4", tag, date, creator, Collections.emptyList(), date, date)
        );
        String expected = objectMapper.writeValueAsString(reminderResponses);
        mockMvc.perform(get(baseUrl)
                        .with(httpBasic(username, password)))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
    }

    @Test
    void getByTag_ShouldReturnListOfReminderWithTag() throws Exception {
        LocalDateTime date = LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0, 0);
        String tag = "tag";
        UserResponseDto creator = new UserResponseDto(1L, "admin", username);
        List<EventReminderResponseDto> reminderResponses = List.of(
                new EventReminderResponseDto(1L, "title1", "text1", tag, date, creator, Collections.emptyList(), date, date),
                new EventReminderResponseDto(2L, "title2", "text2", tag, date, creator, Collections.emptyList(), date, date),
                new EventReminderResponseDto(3L, "title3", "text3", tag, date, creator, Collections.emptyList(), date, date),
                new EventReminderResponseDto(4L, "title4", "text4", tag, date, creator, Collections.emptyList(), date, date)
        );
        String expected = objectMapper.writeValueAsString(reminderResponses);
        mockMvc.perform(get(baseUrl + "tag/" + tag)
                        .with(httpBasic(username, password)))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
    }

    @Test
    void updateReminderAndGet_ShouldReturnUpdatedReminder() throws Exception {
        Long id = 1L;
        String title = "new title";
        String text = "new text";
        String tag = "new tag";
        LocalDateTime date = LocalDateTime.now().withNano(0);
        UserResponseDto creator = new UserResponseDto(1L, "admin", username);
        EventReminderUpdateDto reminderUpdateDto = new EventReminderUpdateDto(id, title, text, tag, date);
        String reminderUpdateDtoJson = objectMapper.writeValueAsString(reminderUpdateDto);

        mockMvc.perform(put(baseUrl).with(httpBasic(username, password))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(reminderUpdateDtoJson))
                .andExpect(status().isOk());

        EventReminderResponseDto expected = new EventReminderResponseDto(id, title, text, tag, date, creator, Collections.emptyList(), null, null);
        String actualJson = mockMvc.perform(get(baseUrl + id).with(httpBasic(username, password)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        EventReminderResponseDto actual = objectMapper.readValue(actualJson, EventReminderResponseDto.class);
        assertThat(actual)
                .usingRecursiveComparison()
                .ignoringFields(createdOnField, lastModifyField)
                .isEqualTo(expected);
    }

    @Test
    void deleteAndGet_ShouldReturnNotFoundError() throws Exception {
        Long id = 1L;
        mockMvc.perform(delete(baseUrl + id).with(httpBasic(username, password)))
                .andExpect(status().isOk());
        mockMvc.perform(get(baseUrl + id).with(httpBasic(username, password)))
                .andExpect(status().isNotFound());
    }
}