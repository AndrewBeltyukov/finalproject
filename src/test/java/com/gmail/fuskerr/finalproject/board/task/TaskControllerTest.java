package com.gmail.fuskerr.finalproject.board.task;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gmail.fuskerr.finalproject.ApplicationTests;
import com.gmail.fuskerr.finalproject.board.status.dto.TaskStatusDto;
import com.gmail.fuskerr.finalproject.board.task.dto.*;
import com.gmail.fuskerr.finalproject.reminder.notification.NotificationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@Transactional
@Sql(scripts = "classpath:board-data.sql")
@Sql(scripts = "classpath:status-data.sql")
@Sql(scripts = "classpath:task-data.sql")
class TaskControllerTest extends ApplicationTests {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    NotificationService notificationService;

    private final String baseUrl = "/board/1/task/";

    private final String username = "admin@email.com";
    private final String password = "password";
    private final String createdOnField = "createdOn";
    private final String lastModifyField = "lastModify";

    @Test
    void createAndGetById_ShouldReturnSame() throws Exception {
        Long id = 5L;
        String title = "title";
        String text = "text";
        String tag = "task";
        TaskStatusDto status = new TaskStatusDto("TEST");
        Long boardId = 1L;

        TaskCreateDto createDto = new TaskCreateDto(title, text, tag, status, null, boardId);
        String createJson = objectMapper.writeValueAsString(createDto);
        TaskResponseDto expected = new TaskResponseDto(id, title, text, tag, status, null, null, null);

        String actualJson = mockMvc.perform(post(baseUrl)
                        .with(httpBasic(username, password))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(createJson))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        TaskResponseDto actual = objectMapper.readValue(actualJson, TaskResponseDto.class);
        assertThat(actual)
                .usingRecursiveComparison()
                .ignoringFields(createdOnField, lastModifyField)
                .isEqualTo(expected);
    }

    @Test
    void getTaskById1_ShouldReturnTaskWithId1() throws Exception {
        Long id = 1L;
        String title = "title1";
        String text = "text1";
        String tag = "task";
        TaskStatusDto status = new TaskStatusDto("TEST");
        LocalDateTime date = LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0, 0);
        TaskResponseDto taskResponseDto = new TaskResponseDto(id, title, text, tag, status, date, date, null);

        String expected = objectMapper.writeValueAsString(taskResponseDto);
        mockMvc.perform(get(baseUrl + id)
                        .with(httpBasic(username, password)))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
    }

    @Test
    void getTasksTagPage2Size2_ShouldReturn2TaskWithTag() throws Exception {
        Long id3 = 3L;
        Long id4 = 4L;
        String title3 = "title3";
        String title4 = "title4";
        String text3 = "text3";
        String text4 = "text4";
        LocalDateTime date = LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0, 0);
        String tag = "task";
        TaskStatusDto status = new TaskStatusDto("TEST");
        List<TaskResponseDto> boardResponses = List.of(
                new TaskResponseDto(id3, title3, text3, tag, status, date, date, null),
                new TaskResponseDto(id4, title4, text4, tag, status, date, date, null)
        );

        String expected = objectMapper.writeValueAsString(boardResponses);
        mockMvc.perform(get(baseUrl + "tag/" + tag + "?page=2&size=2")
                        .with(httpBasic(username, password)))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
    }

    @Test
    void getTasksOnBoard_ShouldReturnListWithTasksOnBoard() throws Exception {
        Long id1 = 1L;
        Long id2 = 2L;
        String title1 = "title1";
        String text1 = "text1";
        String title2 = "title2";
        String text2 = "text2";
        String tag = "task";
        TaskStatusDto status = new TaskStatusDto("TEST");
        LocalDateTime date = LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0, 0);
        List<TaskResponseDto> tasksDto = List.of(
                new TaskResponseDto(id1, title1, text1, tag, status, date, date, null),
                new TaskResponseDto(id2, title2, text2, tag, status, date, date, null)
        );

        String expected = objectMapper.writeValueAsString(tasksDto);
        mockMvc.perform(get(baseUrl)
                        .with(httpBasic(username, password)))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
    }

    @Test
    void migrateTaskToAnotherBoardAndGetByBoardId_ShouldReturnTaskOnAnotherBoard() throws Exception {
        Long id = 1L;
        String title = "title1";
        String text = "text1";
        String tag = "task";
        TaskStatusDto status = new TaskStatusDto("TEST");
        LocalDateTime date = LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0, 0);
        Long boardId = 3L;

        TaskMigrateDto taskMigrateDto = new TaskMigrateDto(id, boardId);
        String taskMigrateDtoJson = objectMapper.writeValueAsString(taskMigrateDto);
        mockMvc.perform(patch(baseUrl + "migrate")
                        .with(httpBasic(username, password))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(taskMigrateDtoJson))
                .andExpect(status().isOk());

        List<TaskResponseDto> taskResponseDto = List.of(
                new TaskResponseDto(id, title, text, tag, status, date, date, null));
        String expected = objectMapper.writeValueAsString(taskResponseDto);
        mockMvc.perform(get("/board/3/task/")
                        .with(httpBasic(username, password)))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
    }

    @Test
    void migrateTaskToBoardWithoutStatus_ShouldReturnNotFoundError() throws Exception {
        Long id = 1L;
        Long boardId = 4L;

        TaskMigrateDto taskMigrateDto = new TaskMigrateDto(id, boardId);
        String taskMigrateDtoJson = objectMapper.writeValueAsString(taskMigrateDto);
        mockMvc.perform(patch(baseUrl + "migrate")
                        .with(httpBasic(username, password))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(taskMigrateDtoJson))
                .andExpect(status().isNotFound());
    }

    @Test
    void changeTaskStatusAndGetById_ShouldReturnTaskWithNewStatus() throws Exception {
        Long id = 1L;
        String title = "title1";
        String text = "text1";
        String tag = "task";
        TaskStatusDto status = new TaskStatusDto("NEW");
        LocalDateTime date = LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0, 0);

        TaskUpdateStatusDto taskUpdateStatusDto = new TaskUpdateStatusDto(id, status);
        String taskUpdateStatusDtoJson = objectMapper.writeValueAsString(taskUpdateStatusDto);
        mockMvc.perform(patch(baseUrl + "status")
                        .with(httpBasic(username, password))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(taskUpdateStatusDtoJson))
                .andExpect(status().isOk());

        TaskResponseDto taskResponseDto = new TaskResponseDto(id, title, text, tag, status, date, date, null);
        String expected = objectMapper.writeValueAsString(taskResponseDto);
        mockMvc.perform(get(baseUrl + id)
                        .with(httpBasic(username, password)))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
    }

    @Test
    void updateTaskAndGet_ShouldReturnUpdatedTask() throws Exception {
        Long id = 1L;
        String title = "new title";
        String text = "new text";
        String tag = "new tag";
        String assignedName = "Name";
        TaskStatusDto status = new TaskStatusDto("TEST");
        LocalDateTime date = LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0, 0);

        TaskUpdateDto taskUpdateDto = new TaskUpdateDto(id, title, text, tag, assignedName);
        String taskUpdateDtoJson = objectMapper.writeValueAsString(taskUpdateDto);
        mockMvc.perform(put(baseUrl)
                        .with(httpBasic(username, password))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(taskUpdateDtoJson))
                .andExpect(status().isOk());

        TaskResponseDto expected = new TaskResponseDto(id, title, text, tag, status, date, date, assignedName);
        String expectedJson = objectMapper.writeValueAsString(expected);
        String actualJson = mockMvc.perform(get(baseUrl + id)
                        .with(httpBasic(username, password)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        TaskResponseDto actual = objectMapper.readValue(actualJson, TaskResponseDto.class);
        assertThat(actual)
                .usingRecursiveComparison()
                .ignoringFields(lastModifyField)
                .isEqualTo(expected);
    }

    @Test
    void deleteTaskAndGet_ShouldReturnNotFoundError() throws Exception {
        Long id = 1L;
        mockMvc.perform(delete(baseUrl + id).with(httpBasic(username, password)))
                .andExpect(status().isOk());
        mockMvc.perform(get(baseUrl + id).with(httpBasic(username, password)))
                .andExpect(status().isNotFound());
    }

    @Test
    void getWithoutAuth_ShouldReturn401Error() throws Exception {
        Long id = 1L;
        mockMvc.perform(get(baseUrl + id).with(httpBasic("unknown@email.com", "password")))
                .andExpect(status().isUnauthorized());
    }

    /*@Test
    void deleteWithUserRole_ShouldReturnForbiddenError() throws Exception {
        Long id = 1L;
        mockMvc.perform(delete(baseUrl + id).with(httpBasic("user@email.com", "password")))
                .andExpect(status().isForbidden());
    }*/
}