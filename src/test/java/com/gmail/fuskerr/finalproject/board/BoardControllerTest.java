package com.gmail.fuskerr.finalproject.board;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gmail.fuskerr.finalproject.ApplicationTests;
import com.gmail.fuskerr.finalproject.board.dto.BoardCreateDto;
import com.gmail.fuskerr.finalproject.board.dto.BoardResponseDto;
import com.gmail.fuskerr.finalproject.board.dto.BoardUpdateDto;
import com.gmail.fuskerr.finalproject.board.status.dto.TaskStatusDto;
import com.gmail.fuskerr.finalproject.reminder.notification.NotificationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@Transactional
class BoardControllerTest extends ApplicationTests {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    NotificationService notificationService;

    private final String baseUrl = "/board/";

    private final String username = "admin@email.com";
    private final String password = "password";
    private final String createdOnField = "createdOn";
    private final String lastModifyField = "lastModify";

    @Test
    void createBoardAndGet_ShouldGetSameBoard() throws Exception {
        Long id = 1L;
        String title = "title";
        String text = "text";
        String tag = "tag";
        List<TaskStatusDto> statusesDto = List.of(new TaskStatusDto("TEST"));
        BoardCreateDto createDto = new BoardCreateDto(title, text, tag, statusesDto);
        String json = objectMapper.writeValueAsString(createDto);

        BoardResponseDto expected = new BoardResponseDto(id, title, text, tag, statusesDto, null, null);
        String actualJson = mockMvc.perform(post(baseUrl)
                        .with(httpBasic(username, password))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        BoardResponseDto actual = objectMapper.readValue(actualJson, BoardResponseDto.class);
        assertThat(actual)
                .usingRecursiveComparison()
                .ignoringFields(createdOnField, lastModifyField)
                .isEqualTo(expected);
    }

    @Test
    @Sql(scripts = "classpath:board-data.sql")
    void getBoardById1_ShouldReturnBoardWithId1() throws Exception {
        Long id = 1L;
        String title = "title1";
        String text = "text1";
        String tag = "tag";
        BoardResponseDto expected = new BoardResponseDto(id, title, text, tag, Collections.emptyList(), null, null);

        String actualJson = mockMvc.perform(get(baseUrl + id)
                        .with(httpBasic(username, password)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        BoardResponseDto actual = objectMapper.readValue(actualJson, BoardResponseDto.class);
        assertThat(actual)
                .usingRecursiveComparison()
                .ignoringFields(createdOnField, lastModifyField)
                .isEqualTo(expected);
    }

    @Test
    @Sql(scripts = "classpath:board-data.sql")
    void getPage2WithSize2_ShouldReturnListOf2Board() throws Exception {
        Long id3 = 3L;
        Long id4 = 4L;
        String title3 = "title3";
        String title4 = "title4";
        String text3 = "text3";
        String text4 = "text4";
        LocalDateTime date = LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0, 0);
        String tag = "tag1";
        List<BoardResponseDto> boardResponses = List.of(
                new BoardResponseDto(id3, title3, text3, tag, Collections.emptyList(), date, date),
                new BoardResponseDto(id4, title4, text4, tag, Collections.emptyList(), date, date)
        );
        String expected = objectMapper.writeValueAsString(boardResponses);
        mockMvc.perform(get(baseUrl + "?page=2&size=2")
                        .with(httpBasic(username, password)))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
    }

    @Test
    @Sql(scripts = "classpath:board-data.sql")
    void getPage2WithSize2ByTag_ShouldReturnListOf2BoardWithTag() throws Exception {
        Long id5 = 5L;
        Long id6 = 6L;
        String title5 = "title5";
        String title6 = "title6";
        String text5 = "text5";
        String text6 = "text6";
        LocalDateTime date = LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0, 0);
        String tag = "tag1";
        List<BoardResponseDto> boardResponses = List.of(
                new BoardResponseDto(id5, title5, text5, tag, Collections.emptyList(), date, date),
                new BoardResponseDto(id6, title6, text6, tag, Collections.emptyList(), date, date)
        );
        String expected = objectMapper.writeValueAsString(boardResponses);
        mockMvc.perform(get(baseUrl + "tag/" + tag + "?page=2&size=2")
                        .with(httpBasic(username, password)))
                .andExpect(status().isOk())
                .andExpect(content().string(expected));
    }

    @Test
    @Sql(scripts = "classpath:board-data.sql")
    void updateBoardAndGet_ShouldReturnUpdatedBoard() throws Exception {
        Long id = 1L;
        String title = "new title";
        String text = "new text";
        String tag = "new tag";
        BoardUpdateDto boardUpdateDto = new BoardUpdateDto(id, title, text, tag);
        String boardUpdateDtoJson = objectMapper.writeValueAsString(boardUpdateDto);
        mockMvc.perform(put(baseUrl).with(httpBasic(username, password))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(boardUpdateDtoJson))
                .andExpect(status().isOk());

        BoardResponseDto expected = new BoardResponseDto(id, title, text, tag, Collections.emptyList(), null, null);

        String actualJson = mockMvc.perform(get(baseUrl + id).with(httpBasic(username, password)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        BoardResponseDto actual = objectMapper.readValue(actualJson, BoardResponseDto.class);

        assertThat(actual)
                .usingRecursiveComparison()
                .ignoringFields(createdOnField, lastModifyField)
                .isEqualTo(expected);
    }

    @Test
    @Sql(scripts = "classpath:board-data.sql")
    void deleteAngGetById_ShouldReturnErrorNotFound() throws Exception {
        Long id = 1L;
        mockMvc.perform(delete(baseUrl + id).with(httpBasic(username, password)))
                .andExpect(status().isOk());
        mockMvc.perform(get(baseUrl + id).with(httpBasic(username, password)))
                .andExpect(status().isNotFound());
    }

    @Test
    void getWithoutAuth_ShouldReturn401Error() throws Exception {
        Long id = 1L;
        mockMvc.perform(get(baseUrl + id).with(httpBasic("unknown@email.com", "password")))
                .andExpect(status().isUnauthorized());
    }

   /* @Test
    void deleteWithUserRole_ShouldReturnForbiddenError() throws Exception {
        Long id = 1L;
        mockMvc.perform(delete(baseUrl + id).with(httpBasic("user@email.com", "password")))
                .andExpect(status().isForbidden());
    }*/
}